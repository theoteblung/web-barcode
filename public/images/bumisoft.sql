-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 15 Des 2014 pada 08.56
-- Versi Server: 5.5.25a
-- Versi PHP: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `bumisoft`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktiva`
--

CREATE TABLE IF NOT EXISTS `aktiva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `coa_nomer` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `nilai_perolehan` double NOT NULL,
  `masa_manfaat` double NOT NULL,
  `masa_manfaat2` double NOT NULL DEFAULT '0',
  `salvage_value` double NOT NULL,
  `tanggal_beli` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan` varchar(255) NOT NULL,
  `supplier_bahan_sparepart_id` int(11) NOT NULL DEFAULT '0',
  `negara` varchar(255) NOT NULL DEFAULT '',
  `saldo_awal` double NOT NULL DEFAULT '0',
  `jumlah_dibayar` double NOT NULL DEFAULT '0',
  `cutoff` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nomer` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktiva_biaya`
--

CREATE TABLE IF NOT EXISTS `aktiva_biaya` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aktiva_id` int(11) NOT NULL,
  `tanggal_biaya` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan_biaya` varchar(255) NOT NULL,
  `jumlah_biaya` double NOT NULL,
  `bukti_bank_coa_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktiva_detail`
--

CREATE TABLE IF NOT EXISTS `aktiva_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aktiva_id` int(11) NOT NULL,
  `tanggal_kontrak` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan_kontrak` varchar(255) NOT NULL,
  `termin_kontrak` int(11) NOT NULL DEFAULT '0',
  `jumlah_kontrak` double NOT NULL,
  `bukti_bank_coa_id` int(11) NOT NULL,
  `termin` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `approval_date`
--

CREATE TABLE IF NOT EXISTS `approval_date` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahan_baku`
--

CREATE TABLE IF NOT EXISTS `bahan_baku` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grup_barang_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipe_barang_id` int(11) NOT NULL,
  `reminder` tinyint(1) NOT NULL,
  `minimum` decimal(15,2) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=112 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahan_jadi`
--

CREATE TABLE IF NOT EXISTS `bahan_jadi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grup_barang_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipe_barang_id` int(11) NOT NULL,
  `reminder` tinyint(1) NOT NULL,
  `minimum` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `tepung_id` int(11) NOT NULL,
  `bbm` tinyint(1) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=50 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahan_sparepart`
--

CREATE TABLE IF NOT EXISTS `bahan_sparepart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grup_barang_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipe_barang_id` int(11) NOT NULL,
  `reminder` tinyint(1) NOT NULL,
  `minimum` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3084 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahan_wip`
--

CREATE TABLE IF NOT EXISTS `bahan_wip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grup_barang_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipe_barang_id` int(11) NOT NULL,
  `reminder` tinyint(1) NOT NULL,
  `minimum` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `batubara`
--

CREATE TABLE IF NOT EXISTS `batubara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grup_barang_id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tipe_barang_id` int(11) NOT NULL,
  `reminder` tinyint(4) NOT NULL,
  `minimum` decimal(15,2) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `blendet_minyak`
--

CREATE TABLE IF NOT EXISTS `blendet_minyak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `blendet_minyak_detail`
--

CREATE TABLE IF NOT EXISTS `blendet_minyak_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blendet_minyak_id` int(11) NOT NULL,
  `lpb_minyak_id` int(11) NOT NULL,
  `tangki_id` int(11) NOT NULL,
  `minyak_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bon_gantung`
--

CREATE TABLE IF NOT EXISTS `bon_gantung` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` decimal(15,2) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `close` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bon_materai`
--

CREATE TABLE IF NOT EXISTS `bon_materai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `keterangan` text NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `budgeting`
--

CREATE TABLE IF NOT EXISTS `budgeting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `budgeting_detail`
--

CREATE TABLE IF NOT EXISTS `budgeting_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `budgeting_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_budget` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jumlah` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bukti_bank`
--

CREATE TABLE IF NOT EXISTS `bukti_bank` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomer_akun` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_bebas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rekening_koran_id` int(11) NOT NULL,
  `bon_gantung_id` int(11) NOT NULL,
  `close_bon_gantung` int(11) NOT NULL DEFAULT '0',
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mk` tinyint(1) NOT NULL,
  `export_faktur` int(11) NOT NULL DEFAULT '0',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11939 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bukti_bank_coa`
--

CREATE TABLE IF NOT EXISTS `bukti_bank_coa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bukti_bank_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debit` decimal(15,2) NOT NULL,
  `kredit` decimal(15,2) NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `aktiva_id` int(11) NOT NULL DEFAULT '0',
  `aktiva_detail_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28929 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bukti_bank_detail`
--

CREATE TABLE IF NOT EXISTS `bukti_bank_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bukti_bank_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nominal` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11939 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cabang`
--

CREATE TABLE IF NOT EXISTS `cabang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `coa`
--

CREATE TABLE IF NOT EXISTS `coa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subhead_coa_id` int(11) NOT NULL,
  `tipe_cash_flow_detil_id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `kode_bank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=249 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `coa_bank`
--

CREATE TABLE IF NOT EXISTS `coa_bank` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `norek` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cabang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `coa_flow`
--

CREATE TABLE IF NOT EXISTS `coa_flow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_db` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `coa_flow_detail`
--

CREATE TABLE IF NOT EXISTS `coa_flow_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coa_flow_id` int(11) NOT NULL,
  `posisi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomer_coa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=233 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `coa_saldo`
--

CREATE TABLE IF NOT EXISTS `coa_saldo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cabang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `saldo_awal` double NOT NULL,
  `debit` double NOT NULL,
  `kredit` double NOT NULL,
  `saldo_akhir` double NOT NULL,
  `referensi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `referensi_id` int(11) NOT NULL DEFAULT '0',
  `referensi_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=136125 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `npwp` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer_packing`
--

CREATE TABLE IF NOT EXISTS `customer_packing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `npwp` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_coa`
--

CREATE TABLE IF NOT EXISTS `cutoff_coa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_coa_detail`
--

CREATE TABLE IF NOT EXISTS `cutoff_coa_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cutoff_coa_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) NOT NULL,
  `debit` double NOT NULL,
  `kredit` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_dp_bahan`
--

CREATE TABLE IF NOT EXISTS `cutoff_dp_bahan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_dp_bahan_detail`
--

CREATE TABLE IF NOT EXISTS `cutoff_dp_bahan_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `cutoff_dp_bahan_id` int(11) NOT NULL,
  `supplier_bahan_baku_id` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `kasir` int(11) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_dp_bahan_direksi`
--

CREATE TABLE IF NOT EXISTS `cutoff_dp_bahan_direksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_dp_bahan_direksi_detail`
--

CREATE TABLE IF NOT EXISTS `cutoff_dp_bahan_direksi_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cutoff_dp_bahan_direksi_id` int(11) NOT NULL,
  `supplier_bahan_baku_id` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_dp_sparepart`
--

CREATE TABLE IF NOT EXISTS `cutoff_dp_sparepart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_dp_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `cutoff_dp_sparepart_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `cutoff_dp_sparepart_id` int(11) NOT NULL,
  `supplier_bahan_sparepart_id` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_stock_sparepart`
--

CREATE TABLE IF NOT EXISTS `cutoff_stock_sparepart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_stock_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `cutoff_stock_sparepart_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cutoff_stock_sparepart_id` int(11) NOT NULL,
  `bahan_sparepart_id` int(11) NOT NULL,
  `nama_excel` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `stock_kartu_sparepart_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=251 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_utang_angkutan_pembelian`
--

CREATE TABLE IF NOT EXISTS `cutoff_utang_angkutan_pembelian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_utang_angkutan_pembelian_detail`
--

CREATE TABLE IF NOT EXISTS `cutoff_utang_angkutan_pembelian_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `cutoff_utang_angkutan_pembelian_id` int(11) NOT NULL,
  `ekspedisi_pembelian_id` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_utang_angkutan_penjualan`
--

CREATE TABLE IF NOT EXISTS `cutoff_utang_angkutan_penjualan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_utang_angkutan_penjualan_detail`
--

CREATE TABLE IF NOT EXISTS `cutoff_utang_angkutan_penjualan_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `cutoff_utang_angkutan_penjualan_id` int(11) NOT NULL,
  `ekspedisi_penjualan_id` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_utang_bahan`
--

CREATE TABLE IF NOT EXISTS `cutoff_utang_bahan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_utang_bahan_detail`
--

CREATE TABLE IF NOT EXISTS `cutoff_utang_bahan_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `cutoff_utang_bahan_id` int(11) NOT NULL,
  `supplier_bahan_baku_id` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_utang_bahan_direksi`
--

CREATE TABLE IF NOT EXISTS `cutoff_utang_bahan_direksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_utang_bahan_direksi_detail`
--

CREATE TABLE IF NOT EXISTS `cutoff_utang_bahan_direksi_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cutoff_utang_bahan_direksi_id` int(11) NOT NULL,
  `supplier_bahan_baku_id` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_utang_sparepart`
--

CREATE TABLE IF NOT EXISTS `cutoff_utang_sparepart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cutoff_utang_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `cutoff_utang_sparepart_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `cutoff_utang_sparepart_id` int(11) NOT NULL,
  `supplier_bahan_sparepart_id` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_angkutan_pembelian`
--

CREATE TABLE IF NOT EXISTS `dp_angkutan_pembelian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ekspedisi_pembelian_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `lebih` tinyint(1) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` double NOT NULL,
  `sisa` double NOT NULL,
  `harijt` double NOT NULL,
  `harium` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `kasir` int(11) NOT NULL DEFAULT '0',
  `bukti_bank_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_angkutan_penjualan`
--

CREATE TABLE IF NOT EXISTS `dp_angkutan_penjualan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ekspedisi_penjualan_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `lebih` tinyint(1) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` double NOT NULL,
  `sisa` double NOT NULL,
  `harijt` double NOT NULL,
  `harium` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `kasir` int(11) NOT NULL DEFAULT '0',
  `bukti_bank_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_bahan`
--

CREATE TABLE IF NOT EXISTS `dp_bahan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `lebih` tinyint(1) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` double NOT NULL,
  `sisa` double NOT NULL,
  `tipe_barang_id` int(11) NOT NULL,
  `harijt` double NOT NULL,
  `harium` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `kasir` int(11) NOT NULL DEFAULT '0',
  `bukti_bank_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1731 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_bahan_detail`
--

CREATE TABLE IF NOT EXISTS `dp_bahan_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dp_bahan_id` int(11) NOT NULL,
  `coa_id` int(11) NOT NULL,
  `uraian` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debit` decimal(15,2) NOT NULL,
  `kredit` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_bahan_direksi`
--

CREATE TABLE IF NOT EXISTS `dp_bahan_direksi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `lebih` tinyint(1) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` double NOT NULL,
  `sisa` double NOT NULL,
  `tipe_barang_id` int(11) NOT NULL,
  `harijt` double NOT NULL,
  `harium` double NOT NULL,
  `tanggal_lunas` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `kasir` int(11) NOT NULL DEFAULT '0',
  `bukti_bank_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_bahan_direksi_detail`
--

CREATE TABLE IF NOT EXISTS `dp_bahan_direksi_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dp_bahan_direksi_id` int(11) NOT NULL,
  `coa_id` int(11) NOT NULL,
  `uraian` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debit` decimal(15,2) NOT NULL,
  `kredit` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_batubara`
--

CREATE TABLE IF NOT EXISTS `dp_batubara` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `supplier_batubara_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `lebih` tinyint(1) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` double NOT NULL,
  `sisa` double NOT NULL,
  `harijt` double NOT NULL,
  `harium` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `kasir` int(11) NOT NULL DEFAULT '0',
  `bukti_bank_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_minyak`
--

CREATE TABLE IF NOT EXISTS `dp_minyak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `lebih` tinyint(1) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` double NOT NULL,
  `sisa` double NOT NULL,
  `harijt` double NOT NULL,
  `harium` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `kasir` int(11) NOT NULL DEFAULT '0',
  `bukti_bank_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_packing`
--

CREATE TABLE IF NOT EXISTS `dp_packing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `supplier_packing_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `lebih` tinyint(1) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` double NOT NULL,
  `sisa` double NOT NULL,
  `harijt` double NOT NULL,
  `harium` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `kasir` int(11) NOT NULL DEFAULT '0',
  `bukti_bank_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_penjualan`
--

CREATE TABLE IF NOT EXISTS `dp_penjualan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `customer_packing_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `lebih` tinyint(1) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` double NOT NULL,
  `sisa` double NOT NULL,
  `harijt` double NOT NULL,
  `harium` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `kasir` int(11) NOT NULL DEFAULT '0',
  `bukti_bank_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_sparepart`
--

CREATE TABLE IF NOT EXISTS `dp_sparepart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_bahan_sparepart_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `lebih` tinyint(1) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` double NOT NULL,
  `sisa` double NOT NULL,
  `tipe_barang_id` int(11) NOT NULL,
  `harijt` double NOT NULL,
  `harium` double NOT NULL,
  `tanggal_lunas` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `kasir` int(11) NOT NULL DEFAULT '0',
  `bukti_bank_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `kpp_id` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `dp_sparepart_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dp_sparepart_id` int(11) NOT NULL,
  `coa_id` int(11) NOT NULL,
  `uraian` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debit` decimal(15,2) NOT NULL,
  `kredit` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dryer`
--

CREATE TABLE IF NOT EXISTS `dryer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=426 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dryer_bahan`
--

CREATE TABLE IF NOT EXISTS `dryer_bahan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dryer_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `zak` double NOT NULL,
  `box` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `berat` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=294 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dryer_hasil`
--

CREATE TABLE IF NOT EXISTS `dryer_hasil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dryer_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stuple` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zak` double NOT NULL,
  `box` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `berat` double NOT NULL,
  `harga` double NOT NULL,
  `shift_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3810 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dryer_kroposan`
--

CREATE TABLE IF NOT EXISTS `dryer_kroposan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dryer_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nokartu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shift_id` int(11) NOT NULL,
  `bruto` double NOT NULL,
  `susut` double NOT NULL,
  `netto` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ekspedisi_pembelian`
--

CREATE TABLE IF NOT EXISTS `ekspedisi_pembelian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `npwp` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ekspedisi_penjualan`
--

CREATE TABLE IF NOT EXISTS `ekspedisi_penjualan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `npwp` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli`
--

CREATE TABLE IF NOT EXISTS `faktur_beli` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `faktur` tinyint(1) NOT NULL,
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL,
  `ppn` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3468 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_batubara`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_batubara` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_batubara_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL,
  `diskon` double NOT NULL,
  `ppn` decimal(15,2) NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `valuta_id2` int(11) NOT NULL,
  `kurs` decimal(15,2) NOT NULL,
  `faktur` tinyint(4) NOT NULL DEFAULT '0',
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_batubara_detail`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_batubara_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `faktur_beli_batubara_id` int(11) NOT NULL,
  `lpb_batubara_id` int(11) NOT NULL,
  `batubara_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `persentase_diskon_item` double NOT NULL,
  `diskon_item` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `lpb_batubara_detail_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_detail`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `faktur_beli_id` int(11) NOT NULL,
  `lpb_bahan_baku_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `stock_kartu_id` int(11) NOT NULL DEFAULT '0',
  `lpb_bahan_baku_detail_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11817 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_direksi`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_direksi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `faktur` tinyint(1) NOT NULL,
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL,
  `ppn` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_direksi_detail`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_direksi_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `faktur_beli_direksi_id` int(11) NOT NULL,
  `lpb_bahan_baku_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `jumlah_daging_merah` double NOT NULL,
  `jumlah_daging` double NOT NULL,
  `jumlah_tulang` double NOT NULL,
  `jumlah_jrh` double NOT NULL,
  `jumlah_tpg` double NOT NULL,
  `jumlah_netraff` double NOT NULL,
  `jumlah_global` double NOT NULL,
  `harga` double NOT NULL,
  `harga_daging` double NOT NULL,
  `harga_tulang` double NOT NULL,
  `pilih` tinyint(4) NOT NULL DEFAULT '0',
  `stock_kartu_id` int(11) NOT NULL,
  `lpb_bahan_baku_detail_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_minyak`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_minyak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL,
  `diskon` double NOT NULL,
  `ppn` decimal(15,2) NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `valuta_id2` int(11) NOT NULL,
  `kurs` decimal(15,2) NOT NULL,
  `faktur` tinyint(4) NOT NULL DEFAULT '0',
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_minyak_detail`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_minyak_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `faktur_beli_minyak_id` int(11) NOT NULL,
  `lpb_minyak_id` int(11) NOT NULL,
  `minyak_id` int(11) NOT NULL,
  `tangki_id` int(11) NOT NULL,
  `jumlah_lpb` double NOT NULL DEFAULT '0',
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `persentase_diskon_item` double NOT NULL,
  `diskon_item` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `lpb_minyak_detail_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_packing`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_packing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_packing_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL,
  `diskon` double NOT NULL,
  `ppn` decimal(15,2) NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `valuta_id2` int(11) NOT NULL,
  `kurs` decimal(15,2) NOT NULL,
  `faktur` tinyint(4) NOT NULL DEFAULT '0',
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_packing_detail`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_packing_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `faktur_beli_packing_id` int(11) NOT NULL,
  `lpb_packing_id` int(11) NOT NULL,
  `packing_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `persentase_diskon_item` double NOT NULL,
  `diskon_item` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `lpb_packing_detail_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=91 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_sparepart`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_sparepart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_sparepart_id` int(11) NOT NULL,
  `supplier_sparepart_id2` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL,
  `diskon` double NOT NULL,
  `ppn` decimal(15,2) NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `valuta_id2` int(11) NOT NULL,
  `kurs` decimal(15,2) NOT NULL,
  `faktur` tinyint(4) NOT NULL DEFAULT '0',
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1362 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_beli_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `faktur_beli_sparepart_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `faktur_beli_sparepart_id` int(11) NOT NULL,
  `lpb_sparepart_id` int(11) NOT NULL,
  `bahan_sparepart_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `persentase_diskon_item` double NOT NULL,
  `diskon_item` double NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `stock_kartu_sparepart_id` int(11) NOT NULL DEFAULT '0',
  `lpb_sparepart_detail_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2374 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `formula`
--

CREATE TABLE IF NOT EXISTS `formula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(50) NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `hasil_produksi` decimal(15,2) NOT NULL,
  `zak` decimal(15,2) NOT NULL,
  `mesin` varchar(255) NOT NULL,
  `mixer` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `finish` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1569 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `formula_detail`
--

CREATE TABLE IF NOT EXISTS `formula_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formula_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stuple` varchar(25) NOT NULL,
  `nokartu` varchar(25) NOT NULL,
  `netawal` double NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `berat` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9106 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `formula_minyak`
--

CREATE TABLE IF NOT EXISTS `formula_minyak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tangki_hasil_id` int(11) NOT NULL,
  `minyak_hasil_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `formula_minyak_detail`
--

CREATE TABLE IF NOT EXISTS `formula_minyak_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `formula_minyak_id` int(11) NOT NULL,
  `tangki_id` int(11) NOT NULL,
  `minyak_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `grup_barang`
--

CREATE TABLE IF NOT EXISTS `grup_barang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kelompok_barang_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `gudang`
--

CREATE TABLE IF NOT EXISTS `gudang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cabang_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `head_coa`
--

CREATE TABLE IF NOT EXISTS `head_coa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kategori_coa_id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `humpb`
--

CREATE TABLE IF NOT EXISTS `humpb` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `posisi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_pengambilan_barang`
--

CREATE TABLE IF NOT EXISTS `jenis_pengambilan_barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurnal_penyesuaian`
--

CREATE TABLE IF NOT EXISTS `jurnal_penyesuaian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurnal_penyesuaian_detail`
--

CREATE TABLE IF NOT EXISTS `jurnal_penyesuaian_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jurnal_penyesuaian_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debit` decimal(15,2) NOT NULL,
  `kredit` decimal(15,2) NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_master_id` int(11) NOT NULL,
  `relevance_master_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=93 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=83 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_coa`
--

CREATE TABLE IF NOT EXISTS `kategori_coa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompok_barang`
--

CREATE TABLE IF NOT EXISTS `kelompok_barang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kirim_minyak`
--

CREATE TABLE IF NOT EXISTS `kirim_minyak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bayar_ongkos` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=79 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kirim_minyak_detail`
--

CREATE TABLE IF NOT EXISTS `kirim_minyak_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kirim_minyak_id` int(11) NOT NULL,
  `tangki_id` int(11) NOT NULL,
  `minyak_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `truk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=107 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `konversi_lpb`
--

CREATE TABLE IF NOT EXISTS `konversi_lpb` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `konversi_sparepart`
--

CREATE TABLE IF NOT EXISTS `konversi_sparepart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bahan_sparepart_id` int(11) NOT NULL,
  `satuan_asal_id` int(11) NOT NULL,
  `satuan_tujuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `koreksi_stock_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `koreksi_stock_bahan_baku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=265 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `koreksi_stock_bahan_baku_detail`
--

CREATE TABLE IF NOT EXISTS `koreksi_stock_bahan_baku_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `koreksi_stock_bahan_baku_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1428 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `koreksi_stock_bahan_jadi`
--

CREATE TABLE IF NOT EXISTS `koreksi_stock_bahan_jadi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `koreksi_stock_bahan_jadi_detail`
--

CREATE TABLE IF NOT EXISTS `koreksi_stock_bahan_jadi_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `koreksi_stock_bahan_jadi_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stock_kartu_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `koreksi_stock_bahan_sparepart`
--

CREATE TABLE IF NOT EXISTS `koreksi_stock_bahan_sparepart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `koreksi_stock_bahan_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `koreksi_stock_bahan_sparepart_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `koreksi_stock_bahan_sparepart_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `bahan_sparepart_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `koreksi_stock_bahan_wip`
--

CREATE TABLE IF NOT EXISTS `koreksi_stock_bahan_wip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=168 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `koreksi_stock_bahan_wip_detail`
--

CREATE TABLE IF NOT EXISTS `koreksi_stock_bahan_wip_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `koreksi_stock_bahan_wip_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `stuple` int(11) NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=342 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kpp`
--

CREATE TABLE IF NOT EXISTS `kpp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jk_depresiasi` decimal(15,2) NOT NULL,
  `kapasitas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `komponen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=341 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lhp`
--

CREATE TABLE IF NOT EXISTS `lhp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `formula_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL DEFAULT '0',
  `hasil_produksi` decimal(15,2) NOT NULL,
  `zak` decimal(15,2) NOT NULL,
  `total_produksi` double NOT NULL DEFAULT '0',
  `sisa_produksi` double NOT NULL DEFAULT '0',
  `zak_outer_id` int(11) NOT NULL,
  `harga_zak_outer` double NOT NULL,
  `jumlah_zak_outer` double NOT NULL,
  `zak_inner_id` int(11) NOT NULL,
  `harga_zak_inner` double NOT NULL,
  `jumlah_zak_inner` double NOT NULL,
  `finish` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2597 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lhp_detail`
--

CREATE TABLE IF NOT EXISTS `lhp_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lhp_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mesin_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shift_id` int(11) NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `jumlah` decimal(15,2) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4442 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `locking`
--

CREATE TABLE IF NOT EXISTS `locking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `referensi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `locking_ibs_detail`
--

CREATE TABLE IF NOT EXISTS `locking_ibs_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locking_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `stock_kartu_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_user`
--

CREATE TABLE IF NOT EXISTS `log_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `referensi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=66973 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `lpb_bahan_baku` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jam_masuk` time NOT NULL DEFAULT '00:00:00',
  `jam_keluar` time NOT NULL DEFAULT '00:00:00',
  `nama_pengawas` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL DEFAULT '1',
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `harijt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `harium` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direksi` tinyint(1) NOT NULL,
  `tanggal_lunas` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan_lunas` text COLLATE utf8_unicode_ci NOT NULL,
  `timbangan_belakang` tinyint(1) NOT NULL,
  `timbangan_depan` tinyint(1) NOT NULL,
  `hasil_lab` tinyint(1) NOT NULL,
  `finalisasi` tinyint(4) NOT NULL,
  `biaya_kirim` decimal(15,2) NOT NULL,
  `nomer_pi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bayar_ongkos` int(11) NOT NULL DEFAULT '0',
  `close` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10909 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb_bahan_baku_detail`
--

CREATE TABLE IF NOT EXISTS `lpb_bahan_baku_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lpb_bahan_baku_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nokartu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL DEFAULT '0',
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan_putra` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `keterangan_totalan` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `keterangan_lab1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `keterangan_lab2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `edit_putra` tinyint(4) NOT NULL DEFAULT '0',
  `jumlah_totalan_tambahan` double NOT NULL DEFAULT '0',
  `box` double NOT NULL,
  `bruto` double NOT NULL,
  `tara` double NOT NULL,
  `netto` double NOT NULL,
  `bkomp` double NOT NULL,
  `konversi_lpb_id` int(11) NOT NULL,
  `bsupp` double NOT NULL,
  `ka` double NOT NULL,
  `pa` double NOT NULL,
  `prot` double NOT NULL,
  `tvbn` double NOT NULL,
  `fat` double NOT NULL,
  `pd` double NOT NULL,
  `ash` double NOT NULL,
  `raf` double NOT NULL,
  `netraf` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `harga` double NOT NULL,
  `pilih` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13414 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb_batubara`
--

CREATE TABLE IF NOT EXISTS `lpb_batubara` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_batubara_id` int(11) NOT NULL,
  `po_batubara_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `surat_jalan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finalisasi` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=283 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb_batubara_detail`
--

CREATE TABLE IF NOT EXISTS `lpb_batubara_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lpb_batubara_id` int(11) NOT NULL,
  `batubara_id` int(11) NOT NULL,
  `jumlah_po` double NOT NULL,
  `jumlah_lpb` double NOT NULL,
  `berat_sj` double NOT NULL,
  `netto` double NOT NULL,
  `kalori` double NOT NULL,
  `raf` double NOT NULL,
  `harga` double NOT NULL,
  `tutup` tinyint(1) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=282 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb_minyak`
--

CREATE TABLE IF NOT EXISTS `lpb_minyak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_lunas` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan_lunas` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=57 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb_minyak_detail`
--

CREATE TABLE IF NOT EXISTS `lpb_minyak_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lpb_minyak_id` int(11) NOT NULL,
  `tangki_id` int(11) NOT NULL,
  `minyak_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah_sj` double NOT NULL,
  `jumlah_lpb` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=56 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb_packing`
--

CREATE TABLE IF NOT EXISTS `lpb_packing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_packing_id` int(11) NOT NULL,
  `po_packing_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `surat_jalan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=105 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb_packing_detail`
--

CREATE TABLE IF NOT EXISTS `lpb_packing_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lpb_packing_id` int(11) NOT NULL,
  `packing_id` int(11) NOT NULL,
  `jumlah_po` double NOT NULL,
  `jumlah_lpb` double NOT NULL,
  `harga` double NOT NULL,
  `tutup` tinyint(1) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=105 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb_sparepart`
--

CREATE TABLE IF NOT EXISTS `lpb_sparepart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_sparepart_id` int(11) NOT NULL,
  `supplier_sparepart_id2` int(11) NOT NULL,
  `po_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `surat_jalan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1432 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `lpb_sparepart_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lpb_sparepart_id` int(11) NOT NULL,
  `bahan_sparepart_id` int(11) NOT NULL,
  `jumlah_po` double NOT NULL,
  `jumlah_lpb` double NOT NULL,
  `harga` double NOT NULL,
  `tutup` tinyint(1) NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2410 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mesin`
--

CREATE TABLE IF NOT EXISTS `mesin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `minyak`
--

CREATE TABLE IF NOT EXISTS `minyak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grup_barang_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `tepung_id` int(11) NOT NULL,
  `bbm` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ongkos_angkutan_lain`
--

CREATE TABLE IF NOT EXISTS `ongkos_angkutan_lain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `nomer` varchar(255) NOT NULL,
  `pembayaran` varchar(255) NOT NULL DEFAULT 'kas',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `ekspedisi_pembelian_id` int(11) NOT NULL,
  `bukti_pelunasan` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ongkos_angkutan_lain_detail`
--

CREATE TABLE IF NOT EXISTS `ongkos_angkutan_lain_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ongkos_angkutan_lain_id` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nopol` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `tambah` double NOT NULL,
  `total` double NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ongkos_angkutan_pembelian`
--

CREATE TABLE IF NOT EXISTS `ongkos_angkutan_pembelian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `nomer` varchar(255) NOT NULL,
  `pembayaran` varchar(255) NOT NULL DEFAULT 'kas',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `ekspedisi_pembelian_id` int(11) NOT NULL,
  `coa_nomer_tambahan` varchar(255) NOT NULL DEFAULT '0',
  `bukti_pelunasan` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ongkos_angkutan_pembelian_detail`
--

CREATE TABLE IF NOT EXISTS `ongkos_angkutan_pembelian_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ongkos_angkutan_pembelian_id` int(11) NOT NULL,
  `lpb_bahan_baku_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `tambah` double NOT NULL,
  `total` double NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=801 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ongkos_angkutan_penjualan`
--

CREATE TABLE IF NOT EXISTS `ongkos_angkutan_penjualan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `nomer` varchar(255) NOT NULL,
  `pembayaran` varchar(255) NOT NULL DEFAULT 'kas',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customer_id` int(11) NOT NULL,
  `ekspedisi_penjualan_id` int(11) NOT NULL,
  `bukti_pelunasan` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ongkos_angkutan_penjualan_detail`
--

CREATE TABLE IF NOT EXISTS `ongkos_angkutan_penjualan_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ongkos_angkutan_penjualan_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `tambah` double NOT NULL,
  `total` double NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=133 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `packing`
--

CREATE TABLE IF NOT EXISTS `packing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `grup_barang_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `tepung_id` int(11) NOT NULL,
  `bbm` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemakaian_batubara`
--

CREATE TABLE IF NOT EXISTS `pemakaian_batubara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemakaian_batubara_detail`
--

CREATE TABLE IF NOT EXISTS `pemakaian_batubara_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pemakaian_batubara_id` int(11) NOT NULL,
  `batubara_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `harga_modif` double NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=95 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemakaian_materai`
--

CREATE TABLE IF NOT EXISTS `pemakaian_materai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `bon_materai_id` int(11) NOT NULL,
  `supplier` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `keterangan` text NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemakaian_packing`
--

CREATE TABLE IF NOT EXISTS `pemakaian_packing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemakaian_packing_detail`
--

CREATE TABLE IF NOT EXISTS `pemakaian_packing_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pemakaian_packing_id` int(11) NOT NULL,
  `packing_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemakaian_sparepart`
--

CREATE TABLE IF NOT EXISTS `pemakaian_sparepart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3105 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemakaian_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `pemakaian_sparepart_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pemakaian_sparepart_id` int(11) NOT NULL,
  `bahan_sparepart_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `stock_kartu_sparepart_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4479 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penambahan_packing_bekas`
--

CREATE TABLE IF NOT EXISTS `penambahan_packing_bekas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penambahan_packing_bekas_detail`
--

CREATE TABLE IF NOT EXISTS `penambahan_packing_bekas_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `penambahan_packing_bekas_id` int(11) NOT NULL,
  `packing_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengambilan_barang_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `pengambilan_barang_bahan_baku` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bayar_ongkos` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengambilan_barang_bahan_baku_detail`
--

CREATE TABLE IF NOT EXISTS `pengambilan_barang_bahan_baku_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pengambilan_barang_bahan_baku_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bkomp` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengambilan_barang_bahan_baku_jenis`
--

CREATE TABLE IF NOT EXISTS `pengambilan_barang_bahan_baku_jenis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pengambilan_barang_bahan_baku_id` int(11) NOT NULL,
  `jenis_pengambilan_barang_id` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengambilan_barang_bahan_jadi`
--

CREATE TABLE IF NOT EXISTS `pengambilan_barang_bahan_jadi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bayar_ongkos` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1137 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengambilan_barang_bahan_jadi_detail`
--

CREATE TABLE IF NOT EXISTS `pengambilan_barang_bahan_jadi_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pengambilan_barang_bahan_jadi_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bkomp` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1906 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengambilan_barang_bahan_jadi_jenis`
--

CREATE TABLE IF NOT EXISTS `pengambilan_barang_bahan_jadi_jenis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pengambilan_barang_bahan_jadi_id` int(11) NOT NULL,
  `jenis_pengambilan_barang_id` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2154 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengambilan_barang_bahan_wip`
--

CREATE TABLE IF NOT EXISTS `pengambilan_barang_bahan_wip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bayar_ongkos` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengambilan_barang_bahan_wip_detail`
--

CREATE TABLE IF NOT EXISTS `pengambilan_barang_bahan_wip_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pengambilan_barang_bahan_wip_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bkomp` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengambilan_barang_bahan_wip_jenis`
--

CREATE TABLE IF NOT EXISTS `pengambilan_barang_bahan_wip_jenis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pengambilan_barang_bahan_wip_id` int(11) NOT NULL,
  `jenis_pengambilan_barang_id` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengawas`
--

CREATE TABLE IF NOT EXISTS `pengawas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengeluaran_zak_lain`
--

CREATE TABLE IF NOT EXISTS `pengeluaran_zak_lain` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_packing_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_lunas` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan_lunas` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengeluaran_zak_lain_detail`
--

CREATE TABLE IF NOT EXISTS `pengeluaran_zak_lain_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pengeluaran_zak_lain_id` int(11) NOT NULL,
  `packing_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengisian_bl_laporan`
--

CREATE TABLE IF NOT EXISTS `pengisian_bl_laporan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah_bl_mentah` double NOT NULL,
  `jumlah_ts_mentah` double NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_bekas`
--

CREATE TABLE IF NOT EXISTS `penjualan_bekas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_packing_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_lunas` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan_lunas` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_bekas_detail`
--

CREATE TABLE IF NOT EXISTS `penjualan_bekas_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `penjualan_bekas_id` int(11) NOT NULL,
  `packing_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_lain`
--

CREATE TABLE IF NOT EXISTS `penjualan_lain` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_packing_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_lunas` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan_lunas` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_lain_detail`
--

CREATE TABLE IF NOT EXISTS `penjualan_lain_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `penjualan_lain_id` int(11) NOT NULL,
  `pengeluaran_zak_lain_id` int(11) NOT NULL DEFAULT '0',
  `pengeluaran_zak_lain_detail_id` int(11) NOT NULL DEFAULT '0',
  `packing_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyusutan`
--

CREATE TABLE IF NOT EXISTS `penyusutan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyusutan_detail`
--

CREATE TABLE IF NOT EXISTS `penyusutan_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penyusutan_id` int(11) NOT NULL,
  `aktiva_id` int(11) NOT NULL DEFAULT '0',
  `jumlah_awal` double NOT NULL DEFAULT '0',
  `jumlah_akhir` double NOT NULL DEFAULT '0',
  `jumlah` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `permissions_name_index` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1051 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_gudang_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `pindah_gudang_bahan_baku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `gudang_asal_id` int(11) NOT NULL,
  `gudang_tujuan_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_gudang_bahan_baku_detail`
--

CREATE TABLE IF NOT EXISTS `pindah_gudang_bahan_baku_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pindah_gudang_bahan_baku_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `stock_kartu_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_gudang_bahan_jadi`
--

CREATE TABLE IF NOT EXISTS `pindah_gudang_bahan_jadi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `gudang_asal_id` int(11) NOT NULL,
  `gudang_tujuan_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_gudang_bahan_jadi_detail`
--

CREATE TABLE IF NOT EXISTS `pindah_gudang_bahan_jadi_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pindah_gudang_bahan_jadi_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `stock_kartu_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_gudang_bahan_sparepart`
--

CREATE TABLE IF NOT EXISTS `pindah_gudang_bahan_sparepart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `gudang_asal_id` int(11) NOT NULL,
  `gudang_tujuan_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_gudang_bahan_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `pindah_gudang_bahan_sparepart_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pindah_gudang_bahan_sparepart_id` int(11) NOT NULL,
  `bahan_sparepart_id` int(11) NOT NULL,
  `stock_kartu_sparepart_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_gudang_bahan_wip`
--

CREATE TABLE IF NOT EXISTS `pindah_gudang_bahan_wip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `gudang_asal_id` int(11) NOT NULL,
  `gudang_tujuan_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_gudang_bahan_wip_detail`
--

CREATE TABLE IF NOT EXISTS `pindah_gudang_bahan_wip_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pindah_gudang_bahan_wip_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `stock_kartu_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_kualitas_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `pindah_kualitas_bahan_baku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_kualitas_bahan_baku_detail`
--

CREATE TABLE IF NOT EXISTS `pindah_kualitas_bahan_baku_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pindah_kualitas_bahan_baku_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `relevance_id_tujuan` int(11) NOT NULL,
  `relevance_table_tujuan` varchar(255) NOT NULL,
  `gudang_id_tujuan` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `stuple_tujuan` int(11) NOT NULL,
  `nokartu_tujuan` int(11) NOT NULL,
  `jumlah_awal` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL,
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `opname` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_kualitas_bahan_jadi`
--

CREATE TABLE IF NOT EXISTS `pindah_kualitas_bahan_jadi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_kualitas_bahan_jadi_detail`
--

CREATE TABLE IF NOT EXISTS `pindah_kualitas_bahan_jadi_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pindah_kualitas_bahan_jadi_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `relevance_id_tujuan` int(11) NOT NULL,
  `relevance_table_tujuan` varchar(255) NOT NULL,
  `gudang_id_tujuan` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `stuple_tujuan` int(11) NOT NULL,
  `nokartu_tujuan` int(11) NOT NULL,
  `spesifikasi_id` int(11) NOT NULL DEFAULT '0',
  `jumlah_awal` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL,
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `opname` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_kualitas_bahan_wip`
--

CREATE TABLE IF NOT EXISTS `pindah_kualitas_bahan_wip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_kualitas_bahan_wip_detail`
--

CREATE TABLE IF NOT EXISTS `pindah_kualitas_bahan_wip_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pindah_kualitas_bahan_wip_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `relevance_id_tujuan` int(11) NOT NULL,
  `relevance_table_tujuan` varchar(255) NOT NULL,
  `gudang_id_tujuan` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `stuple_tujuan` int(11) NOT NULL,
  `nokartu_tujuan` int(11) NOT NULL,
  `spesifikasi_id` int(11) NOT NULL DEFAULT '0',
  `jumlah_awal` double NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL,
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `opname` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_minyak`
--

CREATE TABLE IF NOT EXISTS `pindah_minyak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_minyak_detail`
--

CREATE TABLE IF NOT EXISTS `pindah_minyak_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pindah_minyak_id` int(11) NOT NULL,
  `tangki_asal_id` int(11) NOT NULL,
  `tangki_tujuan_id` int(11) NOT NULL,
  `minyak_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `piutang`
--

CREATE TABLE IF NOT EXISTS `piutang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `piutang_awal` double NOT NULL,
  `jumlah` double NOT NULL,
  `piutang_akhir` double NOT NULL,
  `referensi` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `piutang_bahan`
--

CREATE TABLE IF NOT EXISTS `piutang_bahan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `total` double NOT NULL,
  `sisa` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `pindah` int(11) NOT NULL DEFAULT '0',
  `kasir` int(11) NOT NULL DEFAULT '0',
  `bukti_bank_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `po`
--

CREATE TABLE IF NOT EXISTS `po` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `pr_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_sparepart_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `rate` decimal(15,2) NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `karyawan_id` int(11) NOT NULL,
  `diskon` decimal(15,2) NOT NULL,
  `ppn` decimal(15,2) NOT NULL,
  `biaya_kirim` decimal(15,2) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1505 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `po_batubara`
--

CREATE TABLE IF NOT EXISTS `po_batubara` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_batubara_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `rate` decimal(15,2) NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `karyawan_id` int(11) NOT NULL,
  `diskon` decimal(15,2) NOT NULL,
  `ppn` decimal(15,2) NOT NULL,
  `biaya_kirim` decimal(15,2) NOT NULL,
  `approval` int(11) NOT NULL DEFAULT '0',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `po_batubara_detail`
--

CREATE TABLE IF NOT EXISTS `po_batubara_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_batubara_id` int(11) NOT NULL,
  `batubara_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `kalori` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `po_detail`
--

CREATE TABLE IF NOT EXISTS `po_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_id` int(11) NOT NULL,
  `bahan_sparepart_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `stock_kartu_sparepart_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2723 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `po_packing`
--

CREATE TABLE IF NOT EXISTS `po_packing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_packing_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `rate` decimal(15,2) NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `karyawan_id` int(11) NOT NULL,
  `diskon` decimal(15,2) NOT NULL,
  `ppn` decimal(15,2) NOT NULL,
  `biaya_kirim` decimal(15,2) NOT NULL,
  `approval` int(11) NOT NULL DEFAULT '0',
  `send_email` int(11) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `direksi_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `po_packing_detail`
--

CREATE TABLE IF NOT EXISTS `po_packing_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_packing_id` int(11) NOT NULL,
  `packing_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_angkutan_pembelian`
--

CREATE TABLE IF NOT EXISTS `pp_angkutan_pembelian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `ekspedisi_pembelian_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faktur` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_angkutan_pembelian_detail`
--

CREATE TABLE IF NOT EXISTS `pp_angkutan_pembelian_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pp_angkutan_pembelian_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=112 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_angkutan_penjualan`
--

CREATE TABLE IF NOT EXISTS `pp_angkutan_penjualan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `ekspedisi_penjualan_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faktur` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_angkutan_penjualan_detail`
--

CREATE TABLE IF NOT EXISTS `pp_angkutan_penjualan_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pp_angkutan_penjualan_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=87 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_bahan`
--

CREATE TABLE IF NOT EXISTS `pp_bahan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faktur` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3297 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_bahan_detail`
--

CREATE TABLE IF NOT EXISTS `pp_bahan_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pp_bahan_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `action` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7313 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_bahan_direksi`
--

CREATE TABLE IF NOT EXISTS `pp_bahan_direksi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL,
  `faktur` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_bahan_direksi_detail`
--

CREATE TABLE IF NOT EXISTS `pp_bahan_direksi_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pp_bahan_direksi_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `action` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_batubara`
--

CREATE TABLE IF NOT EXISTS `pp_batubara` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `supplier_batubara_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faktur` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_batubara_detail`
--

CREATE TABLE IF NOT EXISTS `pp_batubara_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pp_batubara_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_lain`
--

CREATE TABLE IF NOT EXISTS `pp_lain` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `supplier_bahan_sparepart_id` int(11) NOT NULL,
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faktur` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4070 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_lain_bank`
--

CREATE TABLE IF NOT EXISTS `pp_lain_bank` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `supplier_bahan_sparepart_id` int(11) NOT NULL,
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faktur` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=480 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_lain_bank_detail`
--

CREATE TABLE IF NOT EXISTS `pp_lain_bank_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pp_lain_bank_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `kpp_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=481 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_lain_detail`
--

CREATE TABLE IF NOT EXISTS `pp_lain_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pp_lain_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `kpp_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4973 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_minyak`
--

CREATE TABLE IF NOT EXISTS `pp_minyak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faktur` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_minyak_detail`
--

CREATE TABLE IF NOT EXISTS `pp_minyak_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pp_minyak_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_packing`
--

CREATE TABLE IF NOT EXISTS `pp_packing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `supplier_packing_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faktur` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_packing_detail`
--

CREATE TABLE IF NOT EXISTS `pp_packing_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pp_packing_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_sparepart`
--

CREATE TABLE IF NOT EXISTS `pp_sparepart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `supplier_bahan_sparepart_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faktur` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1429 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pp_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `pp_sparepart_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pp_sparepart_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1796 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pr`
--

CREATE TABLE IF NOT EXISTS `pr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skip` tinyint(1) NOT NULL,
  `supplier_sparepart_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `pengawas_id` int(11) NOT NULL DEFAULT '0',
  `karyawan_approval_id` int(11) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `ttd` text COLLATE utf8_unicode_ci NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `mandor_id` int(11) NOT NULL DEFAULT '0',
  `approval_mandor` int(11) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1528 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pr_detail`
--

CREATE TABLE IF NOT EXISTS `pr_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pr_id` int(11) NOT NULL,
  `bahan_sparepart_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `stock_kartu_sparepart_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2578 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pt_packing`
--

CREATE TABLE IF NOT EXISTS `pt_packing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archive` int(11) NOT NULL DEFAULT '0',
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pembayaran` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'kas',
  `customer_packing_id` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `direksi_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL DEFAULT '0',
  `direksi_id2` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `jenis_media_id` int(11) NOT NULL,
  `tanggal_approve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bukti_pelunasan` tinyint(4) NOT NULL,
  `approval` tinyint(4) NOT NULL DEFAULT '1',
  `send_email` tinyint(4) NOT NULL DEFAULT '0',
  `tanggal_submit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pt_packing_detail`
--

CREATE TABLE IF NOT EXISTS `pt_packing_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pt_packing_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `kpp_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekening_koran`
--

CREATE TABLE IF NOT EXISTS `rekening_koran` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `debit` decimal(15,2) NOT NULL,
  `kredit` decimal(15,2) NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `close` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pembelian_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `retur_pembelian_bahan_baku` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bayar_ongkos` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pembelian_bahan_baku_detail`
--

CREATE TABLE IF NOT EXISTS `retur_pembelian_bahan_baku_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `retur_pembelian_bahan_baku_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bkomp` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pengambilan_barang_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `retur_pengambilan_barang_bahan_baku` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bayar_ongkos` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pengambilan_barang_bahan_baku_detail`
--

CREATE TABLE IF NOT EXISTS `retur_pengambilan_barang_bahan_baku_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `retur_pengambilan_barang_bahan_baku_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bkomp` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pengambilan_barang_bahan_baku_jenis`
--

CREATE TABLE IF NOT EXISTS `retur_pengambilan_barang_bahan_baku_jenis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `retur_pengambilan_barang_bahan_baku_id` int(11) NOT NULL,
  `jenis_retur_pengambilan_barang_id` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pengambilan_barang_bahan_jadi`
--

CREATE TABLE IF NOT EXISTS `retur_pengambilan_barang_bahan_jadi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bayar_ongkos` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=111 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pengambilan_barang_bahan_jadi_detail`
--

CREATE TABLE IF NOT EXISTS `retur_pengambilan_barang_bahan_jadi_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `retur_pengambilan_barang_bahan_jadi_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bkomp` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=111 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pengambilan_barang_bahan_jadi_jenis`
--

CREATE TABLE IF NOT EXISTS `retur_pengambilan_barang_bahan_jadi_jenis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `retur_pengambilan_barang_bahan_jadi_id` int(11) NOT NULL,
  `jenis_pengambilan_barang_id` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=179 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pengambilan_barang_bahan_wip`
--

CREATE TABLE IF NOT EXISTS `retur_pengambilan_barang_bahan_wip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bayar_ongkos` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pengambilan_barang_bahan_wip_detail`
--

CREATE TABLE IF NOT EXISTS `retur_pengambilan_barang_bahan_wip_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `retur_pengambilan_barang_bahan_wip_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `spesifikasi_id` int(11) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bkomp` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_pengambilan_barang_bahan_wip_jenis`
--

CREATE TABLE IF NOT EXISTS `retur_pengambilan_barang_bahan_wip_jenis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `retur_pengambilan_barang_bahan_wip_id` int(11) NOT NULL,
  `jenis_retur_pengambilan_barang_id` int(11) NOT NULL,
  `perzak` double NOT NULL,
  `zak` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_data` int(11) NOT NULL DEFAULT '365',
  `level` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `roles_name_index` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_aktiva`
--

CREATE TABLE IF NOT EXISTS `role_aktiva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coa_nomer` varchar(255) NOT NULL,
  `masa_manfaat` double NOT NULL,
  `coa_penyusutan_nomer` varchar(255) NOT NULL,
  `coa_penyusutan_admin_nomer` varchar(255) NOT NULL,
  `coa_penyusutan_produksi_nomer` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `role_user_user_id_index` (`user_id`),
  KEY `role_user_role_id_index` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `separator_minyak`
--

CREATE TABLE IF NOT EXISTS `separator_minyak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `separator_minyak_detail`
--

CREATE TABLE IF NOT EXISTS `separator_minyak_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `separator_minyak_id` int(11) NOT NULL,
  `tangki_id` int(11) NOT NULL,
  `minyak_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `shift`
--

CREATE TABLE IF NOT EXISTS `shift` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `spesifikasi`
--

CREATE TABLE IF NOT EXISTS `spesifikasi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `harga` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=190 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_bahan`
--

CREATE TABLE IF NOT EXISTS `stock_bahan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `referensi` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stock_kartu_id` int(11) NOT NULL,
  `harga_beli` double NOT NULL,
  `hpp` double NOT NULL,
  `stock_awal` double NOT NULL,
  `jumlah` double NOT NULL,
  `stock_akhir` double NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `referensi_id` int(11) NOT NULL DEFAULT '0',
  `referensi_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `supplier_bahan_baku_id` int(11) NOT NULL DEFAULT '0',
  `spesifikasi_id` int(11) NOT NULL DEFAULT '0',
  `zak` double NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=63159 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_batubara`
--

CREATE TABLE IF NOT EXISTS `stock_batubara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referensi` varchar(255) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `batubara_id` int(11) NOT NULL,
  `harga_beli` double NOT NULL,
  `hpp` double NOT NULL,
  `stock_awal` double NOT NULL,
  `jumlah` double NOT NULL,
  `stock_akhir` double NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `referensi_id` int(11) NOT NULL,
  `referensi_table` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=172 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_kartu`
--

CREATE TABLE IF NOT EXISTS `stock_kartu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `stock` double NOT NULL,
  `supplier_bahan_baku_id` int(11) NOT NULL DEFAULT '0',
  `spesifikasi_id` int(11) NOT NULL DEFAULT '0',
  `cutoff` int(11) NOT NULL DEFAULT '0',
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2608 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_kartu_sparepart`
--

CREATE TABLE IF NOT EXISTS `stock_kartu_sparepart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bahan_sparepart_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `stock` double NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3809 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_kpp`
--

CREATE TABLE IF NOT EXISTS `stock_kpp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kpp_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `referensi` varchar(255) NOT NULL,
  `referensi_id` int(11) NOT NULL DEFAULT '0',
  `referensi_table` varchar(255) NOT NULL DEFAULT '',
  `keterangan` varchar(255) NOT NULL,
  `status_print` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29219 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_materai`
--

CREATE TABLE IF NOT EXISTS `stock_materai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) NOT NULL,
  `stock_materai_awal` double NOT NULL,
  `jumlah` double NOT NULL,
  `stock_materai_akhir` double NOT NULL,
  `referensi` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_minyak`
--

CREATE TABLE IF NOT EXISTS `stock_minyak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referensi` varchar(255) NOT NULL,
  `tangki_id` int(11) NOT NULL,
  `minyak_id` int(11) NOT NULL,
  `harga_beli` double NOT NULL,
  `hpp` double NOT NULL,
  `stock_awal` double NOT NULL,
  `jumlah` double NOT NULL,
  `stock_akhir` double NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `referensi_id` int(11) NOT NULL,
  `referensi_table` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=278 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `stock_opname_bahan_baku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `supplier_bahan_baku_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=102 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_bahan_baku_detail`
--

CREATE TABLE IF NOT EXISTS `stock_opname_bahan_baku_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_opname_bahan_baku_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `jumlah_awal` double NOT NULL,
  `hpp_awal` double NOT NULL,
  `jumlah_akhir` double NOT NULL,
  `hpp_akhir` double NOT NULL,
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `opname` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=549 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_bahan_jadi`
--

CREATE TABLE IF NOT EXISTS `stock_opname_bahan_jadi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_bahan_jadi_detail`
--

CREATE TABLE IF NOT EXISTS `stock_opname_bahan_jadi_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_opname_bahan_jadi_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `spesifikasi_id` int(11) NOT NULL DEFAULT '0',
  `jumlah_awal` double NOT NULL,
  `hpp_awal` double NOT NULL,
  `jumlah_akhir` double NOT NULL,
  `hpp_akhir` double NOT NULL,
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `opname` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_bahan_sparepart`
--

CREATE TABLE IF NOT EXISTS `stock_opname_bahan_sparepart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=249 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_bahan_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `stock_opname_bahan_sparepart_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_opname_bahan_sparepart_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `bahan_sparepart_id` int(11) NOT NULL,
  `stock_kartu_sparepart_id` int(11) NOT NULL,
  `jumlah_awal` double NOT NULL,
  `hpp_awal` double NOT NULL,
  `jumlah_akhir` double NOT NULL,
  `hpp_akhir` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1022 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_bahan_wip`
--

CREATE TABLE IF NOT EXISTS `stock_opname_bahan_wip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_bahan_wip_detail`
--

CREATE TABLE IF NOT EXISTS `stock_opname_bahan_wip_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_opname_bahan_wip_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `stuple` int(11) NOT NULL,
  `nokartu` int(11) NOT NULL,
  `spesifikasi_id` int(11) NOT NULL DEFAULT '0',
  `jumlah_awal` double NOT NULL,
  `hpp_awal` double NOT NULL,
  `jumlah_akhir` double NOT NULL,
  `hpp_akhir` double NOT NULL,
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `opname` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=173 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_batubara`
--

CREATE TABLE IF NOT EXISTS `stock_opname_batubara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_batubara_detail`
--

CREATE TABLE IF NOT EXISTS `stock_opname_batubara_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_opname_batubara_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `batubara_id` int(11) NOT NULL,
  `jumlah_awal` double NOT NULL,
  `hpp_awal` double NOT NULL,
  `jumlah_akhir` double NOT NULL,
  `hpp_akhir` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_minyak`
--

CREATE TABLE IF NOT EXISTS `stock_opname_minyak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_minyak_detail`
--

CREATE TABLE IF NOT EXISTS `stock_opname_minyak_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_opname_minyak_id` int(11) NOT NULL,
  `tangki_id` int(11) NOT NULL,
  `minyak_id` int(11) NOT NULL,
  `jumlah_awal` double NOT NULL,
  `hpp_awal` double NOT NULL,
  `jumlah_akhir` double NOT NULL,
  `hpp_akhir` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_packing`
--

CREATE TABLE IF NOT EXISTS `stock_opname_packing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_opname_packing_detail`
--

CREATE TABLE IF NOT EXISTS `stock_opname_packing_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_opname_packing_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `packing_id` int(11) NOT NULL,
  `jumlah_awal` double NOT NULL,
  `hpp_awal` double NOT NULL,
  `jumlah_akhir` double NOT NULL,
  `hpp_akhir` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_packing`
--

CREATE TABLE IF NOT EXISTS `stock_packing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referensi` varchar(255) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `packing_id` int(11) NOT NULL,
  `harga_beli` double NOT NULL,
  `hpp` double NOT NULL,
  `stock_awal` double NOT NULL,
  `jumlah` double NOT NULL,
  `stock_akhir` double NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `referensi_id` int(11) NOT NULL,
  `referensi_table` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15936 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_penyusutan`
--

CREATE TABLE IF NOT EXISTS `stock_penyusutan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `aktiva_id` int(11) NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `stock_penyusutan_awal` double NOT NULL,
  `jumlah` double NOT NULL,
  `stock_penyusutan_akhir` double NOT NULL,
  `referensi` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_sparepart`
--

CREATE TABLE IF NOT EXISTS `stock_sparepart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `referensi` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `bahan_sparepart_id` int(11) NOT NULL,
  `stock_kartu_sparepart_id` int(11) NOT NULL,
  `harga_beli` double NOT NULL,
  `hpp` double NOT NULL,
  `stock_awal` double NOT NULL,
  `jumlah` double NOT NULL,
  `stock_akhir` double NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `referensi_id` int(11) NOT NULL DEFAULT '0',
  `referensi_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11992 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `subhead_coa`
--

CREATE TABLE IF NOT EXISTS `subhead_coa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_coa_id` int(11) NOT NULL,
  `humpb_id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=115 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `supplier_bahan_baku` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `norek` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kontak` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=308 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier_batubara`
--

CREATE TABLE IF NOT EXISTS `supplier_batubara` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `norek` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kontak` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier_packing`
--

CREATE TABLE IF NOT EXISTS `supplier_packing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `norek` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kontak` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier_sparepart`
--

CREATE TABLE IF NOT EXISTS `supplier_sparepart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `norek` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kontak` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1049 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tangki`
--

CREATE TABLE IF NOT EXISTS `tangki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `biaya` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tepung`
--

CREATE TABLE IF NOT EXISTS `tepung` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `kode` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_barang`
--

CREATE TABLE IF NOT EXISTS `tipe_barang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kelompok_barang_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tolakan_minyak`
--

CREATE TABLE IF NOT EXISTS `tolakan_minyak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `nopol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bayar_ongkos` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tolakan_minyak_detail`
--

CREATE TABLE IF NOT EXISTS `tolakan_minyak_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tolakan_minyak_id` int(11) NOT NULL,
  `tangki_id` int(11) NOT NULL,
  `minyak_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` double NOT NULL,
  `truk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tutup_buku`
--

CREATE TABLE IF NOT EXISTS `tutup_buku` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tutup_buku_detail`
--

CREATE TABLE IF NOT EXISTS `tutup_buku_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tutup_buku_id` int(11) NOT NULL,
  `coa_nomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debit` decimal(15,2) NOT NULL,
  `kredit` decimal(15,2) NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `uangmuka`
--

CREATE TABLE IF NOT EXISTS `uangmuka` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `uangmuka_awal` double NOT NULL,
  `jumlah` double NOT NULL,
  `uangmuka_akhir` double NOT NULL,
  `referensi` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4049 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_username_index` (`username`),
  KEY `users_password_index` (`password`),
  KEY `users_email_index` (`email`),
  KEY `users_deleted_at_index` (`deleted_at`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=44 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `utang`
--

CREATE TABLE IF NOT EXISTS `utang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `relevance_id` int(11) NOT NULL,
  `relevance_table` varchar(255) NOT NULL,
  `nomer` varchar(255) NOT NULL,
  `utang_awal` double NOT NULL,
  `jumlah` double NOT NULL,
  `utang_akhir` double NOT NULL,
  `referensi` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11458 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `valuta`
--

CREATE TABLE IF NOT EXISTS `valuta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
