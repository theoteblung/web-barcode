;(function($){
	// Default Options.
	var defaults = {
		searchId: '#searchfield',
		dropdownId: '#typeahead',
		serviceUrl: "/",
		requestType: 'GET'
	};
	// Typeahead function definitions
	$.fn.typeahead = function(options){
		// Give preference to user's options.
		var config = $.extend({}, defaults, options);
		// Identify Options.
		var search = $(config.searchId);
		var dropdown = $(config.dropdownId);
		var url = config.serviceUrl;
		var rtype = config.requestType;
		// TODO: Validate User Options.
		console.log(search + "-" + dropdown + "-" + url + "-" + rtype);
		// Hide dropdown initially.
		dropdown.hide();
		// Process typeahead based on events
		search.on({
			focus:function(e){
				if(this.val != ''){	dropdown.slideDown('slow'); }
			},
			blur:function(e){
				dropdown.slideUp('slow');
			},
			keydown:function(e){
				switch(e.keyCode){
					case 40:
						e.preventDefault;
						dropdown.child('li.active').removeClass('active').next().addClass('active');
						break;
					case 38:
						e.preventDefault;
						dropdown.child('li.active').removeClass('active').prev().addClass('active');
						break;
					case 27:
						e.preventDefault;
						search.blur();
						dropdown.hide('slow');
						e.stopPropagation;
						break;
					case 13:
						e.preventDefault;
						this.form.submit();
						break;
				}
			},
			keyup:function(e){
				switch(e.keyCode){
					case 40:
					case 38:
					case 27:
					case 13:
						break;
					default:
						if(search.val() == ''){
							dropdown.hide('slow');
						}
						else{
							dropdown.slideDown('slow');
							dropdown.css('max-width',search.width()+12);
							dropdown.css('min-width',search.width()+12);
							$.post(url + search.val(), function(data){
								dropdown.child().remove();
								dropdown.append('<li class="active"><a href="search/results">' + search.val() + '</a></li>');
								var arr = data.split(',');
								if(arr.length > 1){
									for(var i=1; i <= arr.length; i++){
										typeahead.append('<li><a href="/products/' + arr[i-1] + '">' + arr[i] + '</a></li>');
										i+=4;
									}
								}
							});
						}
				}
			}
		});
	};
}(jQuery));