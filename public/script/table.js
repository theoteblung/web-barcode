/*
 * MoonCake v1.3.1 - DataTables Demo JS
 *
 * This file is part of MoonCake, an Admin template build for sale at ThemeForest.
 * For questions, suggestions or support request, please mail me at maimairel@yahoo.com
 *
 * Development Started:
 * July 28, 2012
 * Last Update:
 * December 07, 2012
 *
 */

 ;(function( $, window, document, undefined ) {

    var demos = {

        dtTableTools: function( target ) {

            if( $.fn.dataTable ) {

                target.dataTable({
                    "sDom": "<'dt_header'<'row-fluid'<'span6'l><'span6'T>>r>t<'dt_footer'<'row-fluid'<'span6'i><'span6'p>>>",
                    "oTableTools": {
                        "sSwfPath": "plugins/datatables/TableTools/swf/copy_csv_xls_pdf.swf", 
                        "aButtons": [
                        {
                            "sExtends": "copy", 
                            "sButtonText": '<i class="icol-clipboard-text"></i> Copy'
                        }, 
                        {
                            "sExtends": "csv", 
                            "sButtonText": '<i class="icol-doc-excel-csv"></i> CSV'
                        }, 
                        {
                            "sExtends": "xls", 
                            "sButtonText": '<i class="icol-doc-excel-table"></i> Excel'
                        },                          
                        {
                            "sExtends": "pdf", 
                            "sButtonText": '<i class="icol-doc-pdf"></i> PDF'
                        }, 
                        {
                            "sExtends": "print", 
                            "sButtonText": '<i class="icol-printer"></i> Print'
                        }
                        ]
                    }
                });
            }
        }, 

        dtFixedColumns: function( target ) {

            if( $.fn.dataTable ) {

                var dt = target.dataTable({
                    "sScrollY": "300px",
                    "sScrollX": "100%",
                    "sScrollXInner": "150%",
                    "bScrollCollapse": true,
                    "bPaginate": false
                });
                new FixedColumns( dt );

            }

        }       
    };
    var tables_pajak = { 
        dtTableTools: function( target ) {  
            if( $.fn.dataTable ) { 
                var oTable = target.dataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": false,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "sDom": "<'dt_header'<'row-fluid'<'span6'l><'span6'f>>r>t<'dt_footer'<'row-fluid'<'span6'i><'span6'p>>>",
                    
                    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                            // Bold the grade for all 'A' grade browsers
                            $(nRow).attr('id','trid'+iDisplayIndex );
                            $('.rowid', nRow).val(iDisplayIndex);
                            $('.rowid', nRow).attr('id','rowid'+iDisplayIndex );
                            $('.item_id', nRow).attr('id','item_id'+iDisplayIndex );
                            $('.item_qty', nRow).attr('id','item_qty'+iDisplayIndex );
                            $('.item_harga', nRow).attr('id','item_harga'+iDisplayIndex );
                            $('.item_tambah', nRow).attr('id','item_tambah'+iDisplayIndex );
                            $('.item_kurang', nRow).attr('id','item_kurang'+iDisplayIndex );
                            $('.item_akhir', nRow).attr('id','item_akhir'+iDisplayIndex );
                            $('.item_stock', nRow).attr('id','item_stock'+iDisplayIndex );
                            $('.item_stuple', nRow).attr('id','item_stuple'+iDisplayIndex );
                            $('.item_kartus', nRow).attr('id','item_kartus'+iDisplayIndex );
                            $('.item_gudang', nRow).attr('id','item_gudang'+iDisplayIndex );
                            $('.item_spesifikasi', nRow).attr('id','item_spesifikasi'+iDisplayIndex );
                            $('.item_satuan', nRow).attr('id','item_satuan'+iDisplayIndex );
                            $('.item_daging_merah', nRow).attr('id','item_daging_merah'+iDisplayIndex );
                            $('.item_daging', nRow).attr('id','item_daging'+iDisplayIndex );
                            $('.item_tulang', nRow).attr('id','item_tulang'+iDisplayIndex );
                            $('.item_netto', nRow).attr('id','item_netto'+iDisplayIndex );
                            $('.item_bsupp', nRow).attr('id','item_bsupp'+iDisplayIndex );
                            $('.item_jrh', nRow).attr('id','item_jrh'+iDisplayIndex );
                            $('.item_tpg', nRow).attr('id','item_tpg'+iDisplayIndex );
                            $('.item_harga_daging', nRow).attr('id','item_harga_daging'+iDisplayIndex );
                            $('.item_harga_tulang', nRow).attr('id','item_harga_tulang'+iDisplayIndex );
                            $('.item_raf', nRow).attr('id','item_raf'+iDisplayIndex );
                            $('.item_netraf', nRow).attr('id','item_netraf'+iDisplayIndex );
                            $('.item_kpp', nRow).attr('id','item_kpp'+iDisplayIndex );
                            $('.item_konversi_lpb', nRow).attr('id','item_konversi_lpb'+iDisplayIndex );
                            $('.item_box', nRow).attr('id','item_box'+iDisplayIndex );
                            $('.item_bruto', nRow).attr('id','item_bruto'+iDisplayIndex );
                            $('.item_tara', nRow).attr('id','item_tara'+iDisplayIndex );
                            $('.item_diskon', nRow).attr('id','item_diskon'+iDisplayIndex );
                            $('.item_persentase_diskon', nRow).attr('id','item_persentase_diskon'+iDisplayIndex );
                            return nRow;
                        }
                    });
            }
            
        }           
    };
    var tables_opname = { 
        dtTableTools: function( target ) {  
            if( $.fn.dataTable ) { 
                var table = target.dataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "bScrollCollapse": true,
                    "sDom": "<'dt_header'<'row-fluid'<'span6'l><'span6'f>>r>t<'dt_footer'<'row-fluid'<'span6'i><'span6'p>>>",
                    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                            // Bold the grade for all 'A' grade browsers
                            $(nRow).attr('id','trid'+iDisplayIndex );
                            $('.rowid', nRow).val(iDisplayIndex);
                            $('.rowid', nRow).attr('id','rowid'+iDisplayIndex );
                            $('.item_id', nRow).attr('id','item_id'+iDisplayIndex );
                            $('.item_qty', nRow).attr('id','item_qty'+iDisplayIndex );
                            $('.item_harga', nRow).attr('id','item_harga'+iDisplayIndex );
                            $('.item_tambah', nRow).attr('id','item_tambah'+iDisplayIndex );
                            $('.item_kurang', nRow).attr('id','item_kurang'+iDisplayIndex );
                            $('.item_akhir', nRow).attr('id','item_akhir'+iDisplayIndex );
                            $('.item_stock', nRow).attr('id','item_stock'+iDisplayIndex );
                            $('.item_stuple', nRow).attr('id','item_stuple'+iDisplayIndex );
                            $('.item_kartus', nRow).attr('id','item_kartus'+iDisplayIndex );
                            $('.item_gudang', nRow).attr('id','item_gudang'+iDisplayIndex );
                            $('.item_spesifikasi', nRow).attr('id','item_spesifikasi'+iDisplayIndex );
                            $('.item_satuan', nRow).attr('id','item_satuan'+iDisplayIndex );
                            $('.item_daging_merah', nRow).attr('id','item_daging_merah'+iDisplayIndex );
                            $('.item_daging', nRow).attr('id','item_daging'+iDisplayIndex );
                            $('.item_tulang', nRow).attr('id','item_tulang'+iDisplayIndex );
                            $('.item_netto', nRow).attr('id','item_netto'+iDisplayIndex );
                            $('.item_bsupp', nRow).attr('id','item_bsupp'+iDisplayIndex );
                            $('.item_jrh', nRow).attr('id','item_jrh'+iDisplayIndex );
                            $('.item_tpg', nRow).attr('id','item_tpg'+iDisplayIndex );
                            $('.item_harga_daging', nRow).attr('id','item_harga_daging'+iDisplayIndex );
                            $('.item_harga_tulang', nRow).attr('id','item_harga_tulang'+iDisplayIndex );
                            $('.item_raf', nRow).attr('id','item_raf'+iDisplayIndex );
                            $('.item_netraf', nRow).attr('id','item_netraf'+iDisplayIndex );
                            $('.item_kpp', nRow).attr('id','item_kpp'+iDisplayIndex );
                            $('.item_konversi_lpb', nRow).attr('id','item_konversi_lpb'+iDisplayIndex );
                            $('.item_box', nRow).attr('id','item_box'+iDisplayIndex );
                            $('.item_bruto', nRow).attr('id','item_bruto'+iDisplayIndex );
                            $('.item_tara', nRow).attr('id','item_tara'+iDisplayIndex );
                            $('.item_diskon', nRow).attr('id','item_diskon'+iDisplayIndex );
                            $('.item_persentase_diskon', nRow).attr('id','item_persentase_diskon'+iDisplayIndex );
                            return nRow;
                        }
                    });
                
            }
            
        }           
    };
    var tables_search = { 
        dtTableTools: function( target ) {  
            if( $.fn.dataTable ) { 
                target.dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "bScrollCollapse": true,
                    "fnInitComplete": function () {
                        new FixedColumns( oTable);
                    },
                    "sDom": "<'dt_header'<'row-fluid'<'span6'l><'span6'f>>r>t<'dt_footer'<'row-fluid'<'span6'i><'span6'p>>>",
                    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                            // Bold the grade for all 'A' grade browsers
                            $(nRow).attr('id','trid'+iDisplayIndex );
                            $('.rowid', nRow).val(iDisplayIndex);
                            $('.rowid', nRow).attr('id','rowid'+iDisplayIndex );
                            $('.item_id', nRow).attr('id','item_id'+iDisplayIndex );
                            $('.item_qty', nRow).attr('id','item_qty'+iDisplayIndex );
                            $('.item_harga', nRow).attr('id','item_harga'+iDisplayIndex );
                            $('.item_tambah', nRow).attr('id','item_tambah'+iDisplayIndex );
                            $('.item_kurang', nRow).attr('id','item_kurang'+iDisplayIndex );
                            $('.item_akhir', nRow).attr('id','item_akhir'+iDisplayIndex );
                            $('.item_stock', nRow).attr('id','item_stock'+iDisplayIndex );
                            $('.item_stuple', nRow).attr('id','item_stuple'+iDisplayIndex );
                            $('.item_kartus', nRow).attr('id','item_kartus'+iDisplayIndex );
                            $('.item_gudang', nRow).attr('id','item_gudang'+iDisplayIndex );
                            $('.item_spesifikasi', nRow).attr('id','item_spesifikasi'+iDisplayIndex );
                            $('.item_satuan', nRow).attr('id','item_satuan'+iDisplayIndex );
                            $('.item_daging_merah', nRow).attr('id','item_daging_merah'+iDisplayIndex );
                            $('.item_daging', nRow).attr('id','item_daging'+iDisplayIndex );
                            $('.item_tulang', nRow).attr('id','item_tulang'+iDisplayIndex );
                            $('.item_netto', nRow).attr('id','item_netto'+iDisplayIndex );
                            $('.item_bsupp', nRow).attr('id','item_bsupp'+iDisplayIndex );
                            $('.item_jrh', nRow).attr('id','item_jrh'+iDisplayIndex );
                            $('.item_tpg', nRow).attr('id','item_tpg'+iDisplayIndex );
                            $('.item_harga_daging', nRow).attr('id','item_harga_daging'+iDisplayIndex );
                            $('.item_harga_tulang', nRow).attr('id','item_harga_tulang'+iDisplayIndex );
                            $('.item_raf', nRow).attr('id','item_raf'+iDisplayIndex );
                            $('.item_netraf', nRow).attr('id','item_netraf'+iDisplayIndex );
                            $('.item_kpp', nRow).attr('id','item_kpp'+iDisplayIndex );
                            $('.item_konversi_lpb', nRow).attr('id','item_konversi_lpb'+iDisplayIndex );
                            $('.item_box', nRow).attr('id','item_box'+iDisplayIndex );
                            $('.item_bruto', nRow).attr('id','item_bruto'+iDisplayIndex );
                            $('.item_tara', nRow).attr('id','item_tara'+iDisplayIndex );
                            $('.item_diskon', nRow).attr('id','item_diskon'+iDisplayIndex );
                            $('.item_persentase_diskon', nRow).attr('id','item_persentase_diskon'+iDisplayIndex );
                            return nRow;
                        }
                    });
            }
            
        }           
    };
    var displayTable = { 
        dtTableTools: function( target ) {             
            if( $.fn.dataTable ) { 
                target.dataTable({
                    "bSort": false,
                    "sDom": "<'dt_header'<'row-fluid'<'span12'f>>r>t<'dt_footer'<'row-fluid'<'span6'><'span6'p>>>"
                }).columnFilter();
            }
            
        }           
    };
    var tables_default_laporan = { 
        dtTableTools: function( target ) { 
            var title_document = "";
            var is_export = 0;
            var is_print = 0;
            if (document.getElementById("title_document")!=null) {
                title_document = document.getElementById("title_document").value;
            }
            if (document.getElementById("is_export")!=null) {
                is_export = document.getElementById("is_export").value;
            }
            if (document.getElementById("is_print")!=null) {
                is_print = document.getElementById("is_print").value;
            }
            if (is_export==1 && is_print==1) {
                if( $.fn.dataTable ) { 
                    target.dataTable({
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "sDom": "<'dt_header'<'row-fluid'<'span6'l><'span6'T>>r>t<'dt_footer'<'row-fluid'<'span6'i><'span6'p>>>",
                        "oTableTools": {
                            "sSwfPath": "http://bumi.biz/TableTools/swf/copy_csv_xls_pdf.swf", 
                            "aButtons": [
                            {
                                "sExtends": "copy", 
                                "sButtonText": '<i class="icol-clipboard-text"></i> Copy'
                            }, 
                            {
                                "sExtends": "csv", 
                                "sButtonText": '<i class="icol-doc-excel-csv"></i> CSV',
                                "sTitle": ""+title_document,
                            }, 
                            {
                                "sExtends": "xls", 
                                "sButtonText": '<i class="icol-doc-excel-table"></i> Excel',
                                "sTitle": ""+title_document,
                            },                          
                            {
                                "sExtends": "pdf", 
                                "sButtonText": '<i class="icol-doc-pdf"></i> PDF',
                                "sTitle": ""+title_document,
                            }, 
                            {
                                "sExtends": "print", 
                                "sButtonText": '<i class="icol-printer"></i> Print'
                            }
                            ]
                        }
                    }).columnFilter();
                }
            }else if (is_export==0 && is_print==1) {
                if( $.fn.dataTable ) { 
                    target.dataTable({
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "sDom": "<'dt_header'<'row-fluid'<'span6'l><'span6'T>>r>t<'dt_footer'<'row-fluid'<'span6'i><'span6'p>>>",
                        "oTableTools": {
                            "sSwfPath": "http://bumi.biz/TableTools/swf/copy_csv_xls_pdf.swf", 
                            "aButtons": [ 
                            {
                                "sExtends": "print", 
                                "sButtonText": '<i class="icol-printer"></i> Print'
                            }
                            ]
                        }
                    }).columnFilter();
                }
            }else if (is_export==1 && is_print==0) {
                if( $.fn.dataTable ) { 
                    target.dataTable({
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "sDom": "<'dt_header'<'row-fluid'<'span6'l><'span6'T>>r>t<'dt_footer'<'row-fluid'<'span6'i><'span6'p>>>",
                        "oTableTools": {
                            "sSwfPath": "http://bumi.biz/TableTools/swf/copy_csv_xls_pdf.swf", 
                            "aButtons": [
                            {
                                "sExtends": "copy", 
                                "sButtonText": '<i class="icol-clipboard-text"></i> Copy'
                            }, 
                            {
                                "sExtends": "csv", 
                                "sButtonText": '<i class="icol-doc-excel-csv"></i> CSV',
                                "sTitle": ""+title_document,
                            }, 
                            {
                                "sExtends": "xls", 
                                "sButtonText": '<i class="icol-doc-excel-table"></i> Excel',
                                "sTitle": ""+title_document,
                            },                          
                            {
                                "sExtends": "pdf", 
                                "sButtonText": '<i class="icol-doc-pdf"></i> PDF',
                                "sTitle": ""+title_document,
                            }
                            ]
                        }
                    }).columnFilter();
                }
            }
        }           
    };
    


    $(document).ready(function() {  
        if($.fn.dataTable) {    
            if ($("table.cart_table")[0]){
                tables_pajak.dtTableTools( $('table.cart_table') );
            }  
            if ($("table.bahan_table")[0]){
                tables_pajak.dtTableTools( $('table.bahan_table') );
            }
            if ($("table.hasil_table")[0]){
                tables_pajak.dtTableTools( $('table.hasil_table') );
            }
            if ($("table.kroposan_table")[0]){
                tables_pajak.dtTableTools( $('table.kroposan_table') );
            }
            if ($("table.displayTable")[0]){
                displayTable.dtTableTools( $('table.displayTable') );
            }
            if ($("table.table_laporan")[0]){
                tables_default_laporan.dtTableTools( $('table.table_laporan') );
            }
            if ($("table.table_opname")[0]){
                tables_opname.dtTableTools( $('table.table_opname') );
            }
            if ($("table.table_search")[0]){
                tables_search.dtTableTools( $('table.table_search') );
            }
            var jumlah_print = 0;
            if (document.getElementById("jumlah_print")!=null) {
                jumlah_print = document.getElementById("jumlah_print").value;
            }
            for (var i=1;i<=jumlah_print;i++) {
                if (document.getElementById("laporan"+i)!=null) {
                
                $('#laporan'+i).dataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "bScrollCollapse": true,
                    "sDom": "<'dt_header'<'row-fluid'<'span6'l><'span6'f>>r>t<'dt_footer'<'row-fluid'<'span6'i><'span6'p>>>",
                    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                            // Bold the grade for all 'A' grade browsers
                            $(nRow).attr('id','trid'+iDisplayIndex );
                            $('.rowid', nRow).val(iDisplayIndex);
                            $('.rowid', nRow).attr('id','rowid'+iDisplayIndex );
                            $('.item_id', nRow).attr('id','item_id'+iDisplayIndex );
                            $('.item_qty', nRow).attr('id','item_qty'+iDisplayIndex );
                            $('.item_harga', nRow).attr('id','item_harga'+iDisplayIndex );
                            $('.item_tambah', nRow).attr('id','item_tambah'+iDisplayIndex );
                            $('.item_kurang', nRow).attr('id','item_kurang'+iDisplayIndex );
                            $('.item_akhir', nRow).attr('id','item_akhir'+iDisplayIndex );
                            $('.item_stock', nRow).attr('id','item_stock'+iDisplayIndex );
                            $('.item_stuple', nRow).attr('id','item_stuple'+iDisplayIndex );
                            $('.item_kartus', nRow).attr('id','item_kartus'+iDisplayIndex );
                            $('.item_gudang', nRow).attr('id','item_gudang'+iDisplayIndex );
                            $('.item_spesifikasi', nRow).attr('id','item_spesifikasi'+iDisplayIndex );
                            $('.item_satuan', nRow).attr('id','item_satuan'+iDisplayIndex );
                            $('.item_daging_merah', nRow).attr('id','item_daging_merah'+iDisplayIndex );
                            $('.item_daging', nRow).attr('id','item_daging'+iDisplayIndex );
                            $('.item_tulang', nRow).attr('id','item_tulang'+iDisplayIndex );
                            $('.item_netto', nRow).attr('id','item_netto'+iDisplayIndex );
                            $('.item_bsupp', nRow).attr('id','item_bsupp'+iDisplayIndex );
                            $('.item_jrh', nRow).attr('id','item_jrh'+iDisplayIndex );
                            $('.item_tpg', nRow).attr('id','item_tpg'+iDisplayIndex );
                            $('.item_harga_daging', nRow).attr('id','item_harga_daging'+iDisplayIndex );
                            $('.item_harga_tulang', nRow).attr('id','item_harga_tulang'+iDisplayIndex );
                            $('.item_raf', nRow).attr('id','item_raf'+iDisplayIndex );
                            $('.item_netraf', nRow).attr('id','item_netraf'+iDisplayIndex );
                            $('.item_kpp', nRow).attr('id','item_kpp'+iDisplayIndex );
                            $('.item_konversi_lpb', nRow).attr('id','item_konversi_lpb'+iDisplayIndex );
                            $('.item_box', nRow).attr('id','item_box'+iDisplayIndex );
                            $('.item_bruto', nRow).attr('id','item_bruto'+iDisplayIndex );
                            $('.item_tara', nRow).attr('id','item_tara'+iDisplayIndex );
                            return nRow;
                        }
                    });
                }
            }
        }
    });

}) (jQuery, window, document);