<!DOCTYPE html>
<html>
<head>
  <?php
  header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache"); ?>
  <title>Program Deposito v0.0.1</title>
  <!-- <title>Program Pati v0.0.1</title> -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
  <!-- Bootstrap -->
  <link href="{{asset('dist/css/bootstrap.css')}}" rel="stylesheet">
  <link href="{{asset('dist/extend/css/jasny-bootstrap.min.css')}}" rel="stylesheet">

  <!-- Plugins -->
  <link href="{{asset('plugins/offcanvas/offcanvas.css')}}" rel="stylesheet">
  <link href="{{asset('plugins/pace/pace.css')}}" rel="stylesheet">
  <link href="{{asset('plugins/select2/select2.css')}}" rel="stylesheet"/> 


  <!-- Custom -->
  <link href="{{asset('style/style.css')}}" rel="stylesheet">
  <style type="text/css" title="currentStyle">
      .FixedHeader_Cloned th { background-color: white; }
    </style>

  @yield('style')
  <?php
  if (Session::get('session_user_id') == "" || Session::get('session_user_id') == NULL) {
    header("Location: ".URL::to('/'));
  } else {
    $user = User::find(Session::get('session_user_id'));
  }
  
  ?>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <nav class="navbar navbar-default" role="navigation">
        <!-- <nav class="navbar navbar-default" role="navigation" style="background-color:red"> -->
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{URL::to('/dashboard')}}">Program Deposito ({{$user->username}})</a>
          <!-- <a class="navbar-brand" href="{{URL::to('/dashboard')}}">Program Pati ({{$user->username}})</a> -->
        </div>

        <!-- Collect the nav links) || $user->can(forms) || $user->can(and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
              <ul class="dropdown-menu"> 
                <li><a href="{{URL::to('/dashboard')}}">Home</a></li>
                <li><a href="{{URL::to('/signin/signout')}}">Logout</a></li>
                
              </ul>
            </li>
            <!-- Master -->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Master <b class="caret"></b></a>
              <ul class="dropdown-menu"> 
                <li><a href="{{URL::to('/bank')}}">Bank</a></li>
                <li><a href="{{URL::to('/bunga')}}">Bunga</a></li>
              </ul>
            </li> 
            <!-- Pembelian -->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Deposito <b class="caret"></b></a>
              <ul class="dropdown-menu"> 
              <li><a href="{{URL::to('/deposito')}}">Deposito</a></li>
                
              </ul>
            </li>

          
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="{{URL::to('/laporan-stock-sparepart')}}">Laporan Stock Sparepart</a></li>
              
            </ul>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </nav>


    <div class="container-fluid">
      @yield('content')         
    </div><!--/.container-->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('script/jquery.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{asset('plugins/datatables/media/js/jquery.js')}}"></script>
    <!-- Include all compiled plugins (below)) || $user->can(or include individual files as needed -->
    <script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('dist/extend/js/jasny-bootstrap.min.js')}}"></script>

    <!-- Plugins -->
    <script src="{{asset('plugins/offcanvas/offcanvas.js')}}"></script>
    <script src="{{asset('plugins/pace/pace.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.js')}}"></script>
    <script src="{{asset('plugins/notifyjs/dist/notify.js')}}"></script>
    <script src="{{asset('plugins/notifyjs/dist/styles/notify-bootstrap.js')}}"></script>
    <script>

    </script>
    <script type="text/javascript" language="javascript" src="{{asset('plugins/datatables/media/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{asset('plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" charset="utf-8" src="{{asset('plugins/datatables/extras/FixedHeader/js/FixedHeader.js')}}"></script>
    <script language="JavaScript">
      document.onkeydown = function (e) {
        if(e.shiftKey && e.which == 222){

          return false;


        }
      }
    </script>
    

    <!-- Custom -->
    <script src="{{asset('script/script.js')}}"></script>
    <script src="{{asset('script/autoNumeric.js')}}"></script>
    @if (Session::has('notify-success')) 
    <script>$.notify("{{Session::get('notify-success')}}", "success")</script> 
    {{ Session::forget('notify-success'); }}
    @endif
    @if (Session::has('notify-error'))    
    <script>$.notify("{{Session::get('notify-error')}}", "error")</script> 
    {{ Session::forget('notify-error'); }}
    @endif
    @yield('script')
  </body>
  </html>