<?php

class Fungsi {

    // --------------------------------------------------------------------
    static function format_money($number, $fractional = false) {
        
        $explode = explode(".", $number); 
        $number = $explode[0];
        if ($fractional) {
            $number = sprintf('%.2f', $number);
            $number = str_replace(".", ",", $number);
        }
        while (true) {
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1.$2', $number);
            if ($replaced != $number) {
                $number = $replaced;
            } else {
                break;
            }
        }
        
        if (empty($explode[1])) { 
            $number = $number . ",00";
            return ($number);
        }else if (strlen($explode[1])==1) { 
            $number = $number . "," . $explode[1]."0";
            return ($number);
        } else {
            $number = $number . "," . $explode[1];
            return $number;
        }
    }
    static function format_money2($number, $fractional = false) {
        
        $explode = explode(".", $number); 
        $number = $explode[0];
        if ($fractional) {
            $number = sprintf('%.2f', $number);
            $number = str_replace(".", ",", $number);
        }
        while (true) {
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1.$2', $number);
            if ($replaced != $number) {
                $number = $replaced;
            } else {
                break;
            }
        }
        
        if (empty($explode[1])) { 
            $number = $number . "";
            return ($number);
        } else {
            $number = $number . "," . $explode[1];
            return $number;
        }
    }
    static function format_money3($number, $fractional = false) {
        // format angka lgs dibulatkan 2 angka dibelakang
        $number = round($number,2);
        $explode = explode(".", $number); 
        $number = $explode[0];
        if ($fractional) {
            $number = sprintf('%.2f', $number);
            $number = str_replace(".", ",", $number);
        }
        while (true) {
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1.$2', $number);
            if ($replaced != $number) {
                $number = $replaced;
            } else {
                break;
            }
        }
        
        if (empty($explode[1])) { 
            $number = $number . ",00";
            return ($number);
        }else if (strlen($explode[1])==1) { 
            $number = $number . "," . $explode[1]."0";
            return ($number);
        } else {
            $number = $number . "," . $explode[1];
            return $number;
        }
    }

    static function humanToMySQLDate($input_date) {
        $send_date = explode(" ", $input_date);
        $month_string = array('', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $index_month = array_search($send_date[1], $month_string);
        if (strlen($index_month) == 1) {
            $index_month = '0' . $index_month;
        }
        return $send_date[2] . '-' . $index_month . '-' . $send_date[0];
    }

    static function mySQLToHumanDate($input_date) {
        $timestamp = strtotime($input_date);
        return date('d F Y',$timestamp);
    }

    static function nomer_format($table, $id, $date_input) {
        $nomer = '';
        $dateform = date("ymd", strtotime($date_input));
        $nomer = $table.'-' . $dateform . '-' . str_pad($id, 4, '0', STR_PAD_LEFT);
        return $nomer;
    }

    static function getTimeNow() {
        date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d %H:%i:%s";
        $time = time();
        return mdate($datestring, $time);
    }

    static function refresh_script() {

        $result="";
        $result.='<script src="'.URL::base().'/js/custom.js"></script>';
        $result.='<script src="'.URL::base().'/plugins/uniform/uniform.js"></script>';
        $result.='<link rel="stylesheet" href="'.URL::base().'/plugins/uniform/css/uniform.default.css" media="screen" />';
        $result.='<script src="'.URL::base().'/plugins/select2/select2.js"></script>';
        $result.='<link href="'.URL::base().'/plugins/select2/select2.css" rel="stylesheet">';

        return $result;
    }
    
    static function get_nomer_format($table, $id) {
        $nomer = '';
        $dateform = date("ymd");
        $nomer = $table.'-' . $dateform . '-' . str_pad($id, 4, '0', STR_PAD_LEFT);
        return $nomer;
    }
    /*
     * FUNGSI NUMERIK KE TERHITUNG
     * (c) 2008-2010 by amarullz@yahoo.com
     *
     */

    static function terbilanggetvalid($str, $from, $to, $min = 1, $max = 9) {
        $val = false;
        $from = ($from < 0) ? 0 : $from;
        for ($i = $from; $i < $to; $i++) {
            if (((int) $str{$i} >= $min) && ((int) $str{$i} <= $max))
                $val = true;
        }
        return $val;
    }

    static function terbilanggetstr($i, $str, $len) {
        $numA = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan");
        $numB = array("", "se", "dua ", "tiga ", "empat ", "lima ", "enam ", "tujuh ", "delapan ", "sembilan ");
        $numC = array("", "satu ", "dua ", "tiga ", "empat ", "lima ", "enam ", "tujuh ", "delapan ", "sembilan ");
        $numD = array(0 => "puluh", 1 => "belas", 2 => "ratus", 4 => "ribu", 7 => "juta", 10 => "milyar", 13 => "triliun");
        $buf = "";
        $pos = $len - $i;
        switch ($pos) {
            case 1:
            if (!Fungsi::terbilanggetvalid($str, $i - 1, $i, 1, 1))
                $buf = $numA[(int) $str{$i}];
            break;
            case 2: case 5: case 8: case 11: case 14:
            if ((int) $str{$i} == 1) {
                if ((int) $str{$i + 1} == 0)
                    $buf = ($numB[(int) $str{$i}]) . ($numD[0]);
                else
                    $buf = ($numB[(int) $str{$i + 1}]) . ($numD[1]);
            }
            else if ((int) $str{$i} > 1) {
                $buf = ($numB[(int) $str{$i}]) . ($numD[0]);
            }
            break;
            case 3: case 6: case 9: case 12: case 15:
            if ((int) $str{$i} > 0) {
                $buf = ($numB[(int) $str{$i}]) . ($numD[2]);
            }
            break;
            case 4: case 7: case 10: case 13:
            if (Fungsi::terbilanggetvalid($str, $i - 2, $i)) {
                if (!Fungsi::terbilanggetvalid($str, $i - 1, $i, 1, 1))
                    $buf = $numC[(int) $str{$i}] . ($numD[$pos]);
                else
                    $buf = $numD[$pos];
            }
            else if ((int) $str{$i} > 0) {
                if ($pos == 4)
                    $buf = ($numB[(int) $str{$i}]) . ($numD[$pos]);
                else
                    $buf = ($numC[(int) $str{$i}]) . ($numD[$pos]);
            }
            break;
        }
        return $buf;
    }

    static function toTerbilang($nominal) {
        $buf = "";
        $str = $nominal . "";
        $len = strlen($str);
        for ($i = 0; $i < $len; $i++) {
            $buf = trim($buf) . " " . Fungsi::terbilanggetstr($i, $str, $len);
        }
        return trim($buf);
    }
    static function romanNumerals($num){
        $n = intval($num);
        $res = '';

        /*** roman_numerals array  ***/
        $roman_numerals = array(
            'M'  => 1000,
            'CM' => 900,
            'D'  => 500,
            'CD' => 400,
            'C'  => 100,
            'XC' => 90,
            'L'  => 50,
            'XL' => 40,
            'X'  => 10,
            'IX' => 9,
            'V'  => 5,
            'IV' => 4,
            'I'  => 1);

        foreach ($roman_numerals as $roman => $number){
            /*** divide to get  matches ***/
            $matches = intval($n / $number);

            /*** assign the roman char * $matches ***/
            $res .= str_repeat($roman, $matches);

            /*** substract from the number ***/
            $n = $n % $number;
        }

        /*** return the res ***/
        return $res;
    }

}