<?php

class UserController extends BaseController { 

    public function getIndex()
    {

        $view = View::make('user.index');  
        $view->list_user = User::all();      
        return $view;
    } 

    public function getCreate()
    {

        $view = View::make('user.create');
        $view->list_user = User::all(); 
        $view->list_role = Role::all();       
        return $view;
    }

    public function getEdit($id)
    {

        $view =  View::make('user.edit');
        $view->list_user = User::all();    
        $view->list_role = Role::all();       
        $view->user = User::find($id);  
        return $view;
    }

    public function getShow($id)
    {

        $view = View::make('user.show'); 
        $view->list_user = User::all();    
        $view->list_role = Role::all();       
        $view->user = User::find($id);         
        return $view;
    } 
    public function getDelete($id)
    {

    }

    public function getBarcode($barcode){
        $user = User::where('barcode', '=', $barcode)->first(); 
        $user->status = 1;
        if ($user->save()) {
            echo json_encode(true);
        }else {
            echo json_encode(true);
            
        }
    }

    public function putUpdate($id){ 
        $user = User::find($id);
        $user->username = Input::get('username');
        if (Input::get('password')!="") {
            $user->password = Input::get('password');
            
        }
        $user->email = Input::get('email');
        if ($user->save()) {
            $user->roles()->sync(array(Input::get('role_id')));
            //create log user
            $log_user = new LogUser;
            $log_user->tanggal = date("Y-m-d H:i:s");
            $log_user->action = "edit";
            $log_user->relevance_table = "user";
            $log_user->relevance_id = $user->id;
            $log_user->user_id = Session::get('session_user_id');
            $log_user->referensi = "";
            $log_user->keterangan = "";
            $log_user->save();
            Session::put('notify-success', 'Data Grup Barang berhasil dirubah');
            return Redirect::to('user/show/'.$user->id);
        }else {
            Session::put('notify-error', 'Data Grup Barang gagal dirubah');
            return Redirect::to('user/edit/'.$user->id);
            
        }
    }

}