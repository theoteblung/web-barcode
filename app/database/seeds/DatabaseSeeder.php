<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');
		$this->call('PermissionTableSeeder');
		$this->command->info('User table seeded!');
	}

}



class UserTableSeeder2 extends Seeder {

	public function run()
	{ 
		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_faktur_beli_sparepart';
		$permission->category = 'pembelian';
		$permission->save();
		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_faktur_beli_sparepart';
		$permission->category = 'pembelian';
		$permission->save();
		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_faktur_beli_sparepart';
		$permission->category = 'pembelian';
		$permission->save();
		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_faktur_beli_sparepart';
		$permission->category = 'pembelian';
		$permission->save();
		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_faktur_beli_sparepart';
		$permission->category = 'pembelian';
		$permission->save();
		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_faktur_beli_sparepart';
		$permission->category = 'pembelian';
		$permission->save();
	}

}

class PermissionTableSeeder extends Seeder {

	public function run()
	{ 
		$list_role = Role::all();
		$list_permission = Permission::all();
		foreach ($list_role as $role) {
			$list_permission_id = array();
			// $user = User::find($role->id);
			// $user->roles()->sync(array($role->id));
			if ($role->id==2) {
				//admin pembelian
				foreach ($list_permission as $permission) {
					if ($permission->id < 6) {
						$list_permission_id[] = $permission->id;

					}
					if ($permission->id >= 10 && $permission->id <= 21) {
						$list_permission_id[] = $permission->id;						
					}
					// if ($permission->id >= 26 && $permission->id <= 69) {
					// 	$list_permission_id[] = $permission->id;						
					// }
					if ($permission->id >= 70 && $permission->id <= 81) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 88 && $permission->id <= 93) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 130 && $permission->id <= 135) {
						$list_permission_id[] = $permission->id;						
					}
				}
				$role->permissions()->sync($list_permission_id);

			}else if ($role->id==3) {
				//direksi pembelian
				foreach ($list_permission as $permission) {
					if ($permission->id >= 2 && $permission->id <= 5) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 10 && $permission->id <= 21) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 26 && $permission->id <= 69) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 26 && $permission->id <= 69) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 82 && $permission->id <= 87) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 94 && $permission->id <= 99) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 136 && $permission->id <= 141) {
						$list_permission_id[] = $permission->id;						
					}
				}
				$role->permissions()->sync($list_permission_id);
				
			}else if ($role->id==4) {
				//admin stock sparepart
				foreach ($list_permission as $permission) {
					if ($permission->id >= 6 && $permission->id <= 9) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 22 && $permission->id <= 25) {
						$list_permission_id[] = $permission->id;						
					}
					// if ($permission->id >= 26 && $permission->id <= 69) {
					// 	$list_permission_id[] = $permission->id;						
					// }
					if ($permission->id >= 100 && $permission->id <= 105) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 112 && $permission->id <= 129) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 142 && $permission->id <= 147) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 184 && $permission->id <= 189) {
						$list_permission_id[] = $permission->id;						
					}
				}
				$role->permissions()->sync($list_permission_id);

				
			}else if ($role->id==5) {
				//admin po sparepart
				foreach ($list_permission as $permission) {
					if ($permission->id >= 6 && $permission->id <= 9) {
						$list_permission_id[] = $permission->id;						
					}
					if ($permission->id >= 22 && $permission->id <= 25) {
						$list_permission_id[] = $permission->id;						
					}
					// if ($permission->id >= 26 && $permission->id <= 69) {
					// 	$list_permission_id[] = $permission->id;						
					// }
					if ($permission->id >= 106 && $permission->id <= 111) {
						$list_permission_id[] = $permission->id;						
					}
				}
				$role->permissions()->sync($list_permission_id);
				
			}else if ($role->id==6) {
				//admin kasir
				foreach ($list_permission as $permission) {
					if ($permission->id >= 148 && $permission->id <= 177) {
						$list_permission_id[] = $permission->id;						
					}
				}
				$role->permissions()->sync($list_permission_id);
			}
			
			
		}
	}

}

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('role_user')->delete();
		DB::table('permission_role')->delete();
		DB::table('users')->delete();
		DB::table('permissions')->delete();
		DB::table('roles')->delete();

		User::create(
			array(
				'username' => 'admin_pembelian',
				'password' => Hash::make('admin_pembelian'),
				'email' => 'admin_pembelian@gmail.com'
				)
			);
		User::create(
			array(
				'username' => 'direksi_pembelian',
				'password' => Hash::make('direksi_pembelian'),
				'email' => 'direksi_pembelian@gmail.com'
				)
			);
		User::create(
			array(
				'username' => 'admin_stock_sp',
				'password' => Hash::make('admin_stock_sp'),
				'email' => 'admin_stock_sp@gmail.com'
				)
			);
		User::create(
			array(
				'username' => 'admin_po_sp',
				'password' => Hash::make('admin_po_sp'),
				'email' => 'admin_po_sp@gmail.com'
				)
			);
		User::create(
			array(
				'username' => 'admin_kasir',
				'password' => Hash::make('admin_kasir'),
				'email' => 'admin_kasir@gmail.com'
				)
			);
		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_supplier_bahan_baku';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_supplier_bahan_baku';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_supplier_bahan_baku';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_supplier_bahan_baku';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_supplier_bahan_sparepart';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_supplier_bahan_sparepart';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'view_supplier_bahan_sparepart';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_supplier_bahan_sparepart';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_bahan_baku';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_bahan_baku';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_bahan_baku';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_bahan_baku';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_bahan_wip';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_bahan_wip';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_bahan_wip';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_bahan_wip';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_bahan_jadi';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_bahan_jadi';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_bahan_jadi';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_bahan_jadi';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_bahan_sparepart';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_bahan_sparepart';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_bahan_sparepart';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_bahan_sparepart';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_shift';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_shift';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_shift';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_shift';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_satuan';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_satuan';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_satuan';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_satuan';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_grup_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_grup_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_grup_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_grup_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_tipe_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_tipe_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_tipe_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_tipe_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_cabang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_cabang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_cabang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_cabang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_gudang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_gudang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_gudang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_gudang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_kelompok_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_kelompok_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_kelompok_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_kelompok_barang';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_kpp';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_kpp';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_kpp';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_kpp';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_payment';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_payment';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_payment';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_payment';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_konversi_lpb';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_konversi_lpb';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_konversi_lpb';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_konversi_lpb';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_valuta';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_valuta';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_valuta';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_valuta';
		$permission->category = 'master';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_lpb_bahan_baku';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_lpb_bahan_baku';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_lpb_bahan_baku';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_lpb_bahan_baku';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_lpb_bahan_baku';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_lpb_bahan_baku';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_faktur_beli';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_faktur_beli';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_faktur_beli';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_faktur_beli';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_faktur_beli';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_faktur_beli';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_faktur_beli_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_faktur_beli_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_faktur_beli_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_faktur_beli_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_faktur_beli_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_faktur_beli_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_dp_bahan';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_dp_bahan';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_dp_bahan';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_dp_bahan';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_dp_bahan';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_dp_bahan';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_dp_bahan_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_dp_bahan_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_dp_bahan_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_dp_bahan_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_dp_bahan_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_dp_bahan_direksi';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_pr';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_pr';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_pr';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_pr';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_pr';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_pr';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_po';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_po';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_po';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_po';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_po';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_po';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_lpb_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_lpb_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_lpb_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_lpb_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_lpb_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_lpb_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_pemakaian_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_pemakaian_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_pemakaian_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_pemakaian_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_pemakaian_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_pemakaian_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_dp_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_dp_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_dp_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_dp_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_dp_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_dp_sparepart';
		$permission->category = 'pembelian';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_pp_bahan';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_pp_bahan';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_pp_bahan';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_pp_bahan';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_pp_bahan';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_pp_bahan';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_pp_bahan_direksi';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_pp_bahan_direksi';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_pp_bahan_direksi';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_pp_bahan_direksi';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_pp_bahan_direksi';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_pp_bahan_direksi';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_pp_lain';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_pp_lain';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_pp_lain';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_pp_lain';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_pp_lain';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_pp_lain';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_bukti_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_bukti_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_bukti_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_bukti_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_bukti_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_bukti_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_bukti_kas';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_bukti_kas';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_bukti_kas';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_bukti_kas';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_bukti_kas';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_bukti_kas';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_coa_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_coa_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_coa_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_coa_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_coa_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_coa_bank';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_bon_gantung';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_bon_gantung';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_bon_gantung';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_bon_gantung';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_bon_gantung';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_bon_gantung';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'add_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'edit_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'show_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'delete_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'print_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$permission = new Toddish\Verify\Models\Permission;
		$permission->name = 'export_pp_sparepart';
		$permission->category = 'kasir';
		$permission->save();

		$role = new Toddish\Verify\Models\Role;
		$role->name = 'Admin Pembelian';
		$role->level = 7;
		$role->save();

		$role = new Toddish\Verify\Models\Role;
		$role->name = 'Direksi Pembelian';
		$role->level = 7;
		$role->save();

		$role = new Toddish\Verify\Models\Role;
		$role->name = 'Admin Stock Sparepart';
		$role->level = 7;
		$role->save();

		$role = new Toddish\Verify\Models\Role;
		$role->name = 'Admin PO Sparepart';
		$role->level = 7;
		$role->save();

		$role = new Toddish\Verify\Models\Role;
		$role->name = 'Admin Kasir';
		$role->level = 7;
		$role->save();
	}

}